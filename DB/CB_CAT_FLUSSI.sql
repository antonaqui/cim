DROP SEQUENCE cim_integration.CB_CAT_FLUSSI_SEQ;

CREATE SEQUENCE cim_integration.CB_CAT_FLUSSI_SEQ START WITH 1 INCREMENT BY 1;

DROP TABLE cim_integration.CB_CAT_FLUSSI;

CREATE TABLE cim_integration.CB_CAT_FLUSSI(
 id_flusso INT PRIMARY KEY
 ,nome_flusso VARCHAR2(30) NOT NULL
 ,data_configurazione DATE DEFAULT SYSDATE
 ,id_utente_inserimento INT
 ,attivo	 CHAR(1) DEFAULT 'Y'
 ,has_period CHAR(1) DEFAULT 'N'
 ,target_table VARCHAR2(30) NOT NULL
 ,naming_convention_file VARCHAR2(255)
 ,classe_runtime VARCHAR2(255)
);


CREATE OR REPLACE TRIGGER cim_integration.CB_CAT_FLUSSI_IDENTITY
BEFORE INSERT ON cim_integration.CB_CAT_FLUSSI
FOR EACH ROW
BEGIN
    :new.id_flusso := CB_CAT_FLUSSI_SEQ.nextval;
END;

INSERT INTO cim_integration.CB_CAT_FLUSSI(nome_flusso,id_utente_inserimento,target_table, naming_convention_file, classe_runtime) VALUES ('CURVE PEG', 1, 'CB_ARCHIVIO_FILE_CURVE_PEC', 'TBD', 'Enel.Indra.CIMBypass.Business.RuntimeExcecution.RuntimeClass.CurvePEGRuntimeExecutor');