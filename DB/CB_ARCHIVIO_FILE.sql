DROP SEQUENCE cim_integration.CB_ARCHIVIO_FILE_SEQ;

CREATE SEQUENCE cim_integration.CB_ARCHIVIO_FILE_SEQ START WITH 1 INCREMENT BY 1;

DROP TABLE cim_integration.CB_ARCHIVIO_FILE;

CREATE TABLE cim_integration.CB_ARCHIVIO_FILE(
 id_archivio INT PRIMARY KEY
 ,nome_file VARCHAR2(100) NOT NULL
 ,mime_file VARCHAR2(30) NOT NULL
 ,byte BLOB NOT NULL
 ,date_caricamento DATE DEFAULT SYSDATE
 ,id_flusso INT NOT NULL
 ,date_flusso_da DATE DEFAULT SYSDATE
 ,date_flusso_a DATE DEFAULT SYSDATE
 ,id_utente_caricamento INT
 ,elaborato CHAR DEFAULT 'N'
 ,date_elaborazione DATE
 ,id_utente_elaborazione INT
);

CREATE OR REPLACE TRIGGER cim_integration.CB_ARCHIVIO_FILE_IDENTITY
BEFORE INSERT ON cim_integration.CB_ARCHIVIO_FILE
FOR EACH ROW
BEGIN
    :new.id_archivio := CB_ARCHIVIO_FILE_SEQ.nextval;
END;