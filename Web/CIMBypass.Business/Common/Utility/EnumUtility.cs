﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Reflection;


namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class EnumUtility
    {
        public static string GetDescription(this Enum en)
        {
            var fields = en.GetType().GetField(en.ToString());
            var attribute = fields == null ? null : fields.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault();
            return attribute == null ? en.ToString() : ((DescriptionAttribute)attribute).Description;
        }


        public static string GetDescription<TEnum>(int? value)
        {
            string toReturn = null;
            if (value.HasValue)
                toReturn = GetDescription((Enum)(object)((TEnum)(object)(value)));  // ugly, but works
            return toReturn;
        }


        public static TEnum GetEnumValueFromDescription<TEnum>(string description)
        {
            var type = typeof(TEnum);
            if (!type.IsEnum)
                throw new ArgumentException("");
            FieldInfo[] fields = type.GetFields();
            var field = fields
                            .SelectMany(f => f.GetCustomAttributes(
                                typeof(DescriptionAttribute), false), (
                                    f, a) => new { Field = f, Att = a })
                            .SingleOrDefault(a => ((DescriptionAttribute)a.Att)
                                .Description == description);
            return field == null ? default(TEnum) : (TEnum)field.Field.GetRawConstantValue();
        }


        
    }
}
