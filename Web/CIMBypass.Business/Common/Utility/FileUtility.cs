﻿using Enel.Indra.CIMBypass.Business.Common.Log;
using System.IO;
using System.Text;


namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class FileUtility
    {
        private static readonly Log4NetLogger _Logger = new Log4NetLogger("FileUtility");

        internal static byte[] getByte(string contenutoFileInFormatoStringa)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(contenutoFileInFormatoStringa);
            return bytes;
        }

        public static string EstraiNomeFile(string httpUdploadedFile)
        {
            string filename = "";
            filename = Path.GetFileName(httpUdploadedFile);
            return filename;
        }

        public static string DaByteATesto(byte[] byteArray)
        {
            string result = Encoding.UTF8.GetString(byteArray);
            return result;
        }
    }
}