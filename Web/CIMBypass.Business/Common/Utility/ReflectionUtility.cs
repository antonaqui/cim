﻿using System;
using System.Linq.Expressions;

namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class ReflectionUtility
    {

        static string PropertyNameFromMemberExpr(MemberExpression expr)
        {
            return expr.Member.Name;
        }

        static string PropertyNameFromUnaryExpr(UnaryExpression expr)
        {
            if (expr.NodeType == ExpressionType.ArrayLength)
                return "Length";

            MemberExpression mem_expr = null;

            if (expr.Operand is MethodCallExpression)
            {
                mem_expr = (expr.Operand as MethodCallExpression).Object as MemberExpression;
            }
            else if (expr.Operand is MemberExpression)
            {
                mem_expr = expr.Operand as MemberExpression;
            }


            return PropertyNameFromMemberExpr(mem_expr);
        }

        static string PropertyNameFromLambdaExpr(LambdaExpression expr)
        {
            if (expr.Body is MemberExpression) return PropertyNameFromMemberExpr(expr.Body as MemberExpression);
            else if (expr.Body is UnaryExpression) return PropertyNameFromUnaryExpr(expr.Body as UnaryExpression);

            throw new NotSupportedException();
        }

        public static string PropertyNameFromExpr<T>(Expression<Func<T>> expr)
        {
            string toReturn = string.Empty;
            if ((expr != null) && (expr.Body is MemberExpression || expr.Body is UnaryExpression))
                toReturn = PropertyNameFromLambdaExpr(expr);

            return toReturn;
        }

        public static string PropertyNameFromExpr<T>(Expression<Func<T, object>> expr)
        {
            string toReturn = string.Empty;
            if ((expr != null) && (expr.Body is MemberExpression || expr.Body is UnaryExpression))
                toReturn = PropertyNameFromLambdaExpr(expr);

            return toReturn;
        }

        public static string PropertyNameFromExpr<T, TProp>(Expression<Func<T, TProp>> expr)
        {
            string toReturn = string.Empty;
            if((expr != null) && (expr.Body is MemberExpression || expr.Body is UnaryExpression))
                toReturn = PropertyNameFromLambdaExpr(expr);

            return toReturn;
        }

        public static bool AreEqual<T1, T2, TProp>(Expression<Func<T1, TProp>> expr1, Expression<Func<T2, object>> expr2)
        {
            return PropertyNameFromLambdaExpr(expr1).Equals(PropertyNameFromLambdaExpr(expr2));
        }

        public static Expression<Func<TModel, object>> PropertyNameToExpression<TModel>(string propertyName)
        {
            Expression<Func<TModel, object>> toReturn = null;

            if (!string.IsNullOrEmpty(propertyName))
            {
                var paramExpr = Expression.Parameter(typeof(TModel), "el");

                Expression member = Expression.PropertyOrField(paramExpr, propertyName);
                Type typeIfNullable = Nullable.GetUnderlyingType(member.Type);
                if (typeIfNullable != null)
                {
                    member = Expression.Call(member, "GetValueOrDefault", Type.EmptyTypes);
                    member = Expression.Convert(member, typeof(object));
                }

                toReturn =  Expression.Lambda<Func<TModel, object>>(member, paramExpr);
            }

            return toReturn;
        }
    }
}