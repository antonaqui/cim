﻿using System;
using Enel.Indra.CIMBypass.Business.Common.Log;

namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class DateUtility
    {
        private static Log4NetLogger _Logger = new Log4NetLogger("DateUtility");
        /// <summary>
        /// return a DateTime. Input string with yyyyMMdd format 
        /// </summary>
        /// <param name="date"></param>
        /// sample
        /// IN date: 20121005            
        /// OUT date: 05/10/2012 00:00:00
        /// <returns></returns>
        public static DateTime FromStringToDate(string date)
        {
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, "date: " + date);
            DateTime dataFormat = new DateTime();
            if (!String.IsNullOrEmpty(date))
                dataFormat = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, "date: " + dataFormat);
            return dataFormat;
        }

        public static DateTime FromStringWithSlashToDate(string date)
        {
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, "date: " + date);
            DateTime dataFormat = new DateTime();
            if (!String.IsNullOrEmpty(date))
                dataFormat = DateTime.ParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, "date: " + dataFormat);

            return dataFormat;
        }

        public static DateTime FromStringWithScoreToDate(string date)
        {
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, "date: " + date);
            DateTime dataFormat = new DateTime();
            if (!String.IsNullOrEmpty(date))
                dataFormat = DateTime.ParseExact(date, "dd-MM-yyyy", System.Globalization.CultureInfo.CurrentCulture);
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, "date: " + dataFormat);

            return dataFormat;
        }

        /// <summary>
        /// FromStringToDateDMY (D=day M=month Y=Year) returns a DateTime. Input string with dd/MM/yyyy format 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime FromStringToDateDMY(string date)
        {
            DateTime dataFormat = new DateTime();
            if (!String.IsNullOrEmpty(date))
            {
                dataFormat = DateTime.ParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
            }

            return dataFormat;
        }

        /// <summary>
        /// FromStringToDateDMY (D=day M=month Y=Year) returns a DateTime. Input string with dd.MM.yyyy format 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime? FromStringToDateDMYPoint(string date)
        {
            DateTime? dataFormat = null;
            if (!String.IsNullOrEmpty(date))
            {
                date = date.Replace(".", "/");
                dataFormat = DateTime.ParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
            }

            return dataFormat;
        }

        /// <summary>
        /// format dd/MM/yyyy
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FromDateToString(DateTime? date)
        {
            DateTime temp = date.GetValueOrDefault();
            string dataFormat = "";

            if (date != null)
            {
                dataFormat = temp.ToString("dd/MM/yyyy");
            }

            return dataFormat;
        }

        /// <summary>
        /// format ddMMyyyy
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FromDateToStringDDMMYYYY(DateTime? date)
        {
            DateTime temp = date.GetValueOrDefault();
            string dataFormat = "";

            if (date != null)
            {
                dataFormat = temp.ToString("ddMMyyyy");
            }

            return dataFormat;
        }

        public static string FromDateToStringDDMMYY(DateTime? date)
        {
            DateTime temp = date.GetValueOrDefault();
            string dataFormat = "";

            if (date != null)
            {
                dataFormat = temp.ToString("ddMMyy");
            }

            return dataFormat;
        }

        /// <summary>
        /// format YYYYMMDD
        /// </summary>
        /// <param name="date">Date to convert in string</param>
        /// <returns>A string, or a void string if the date is null, format yyyy-MM-dd</returns>
        public static string FromDateToStringYYYYMMDD(DateTime? date)
        {
            DateTime temp = date.GetValueOrDefault();
            string dataFormat = "";

            if (date != null)
            {
                dataFormat = temp.ToString("yyyy-MM-dd");
            }

            return dataFormat;
        }

        public static string FromDateToStringShortYYYYMMDD(DateTime? date)
        {
            DateTime temp = date.GetValueOrDefault();
            string dataFormat = "";

            if (date != null)
            {
                dataFormat = temp.ToString("yyyyMMdd");
            }

            return dataFormat;
        }

        /// <summary>
        /// Converte una DateTime in una Date in stringa
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FromDateTimeToShortDateString(DateTime? date)
        {
            return date.HasValue ? date.Value.ToShortDateString() : "";
        }

        /// <summary>
        /// today date format ddMMyyyy
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string TodayDDMMYYYY()
        {
            DateTime temp = DateTime.Now;
            string dataFormat = "";
            dataFormat = temp.ToString("ddMMyyyy");
            return dataFormat;
        }

        /// <summary>
        /// Restituisce una data nel formato 2008-03-09T16:05:07Z
        /// </summary>
        /// <param name="date">La data da formattare</param>
        /// <returns>La data formattata</returns>
        public static string formatToSortableDateTimeWithZ(DateTime date)
        {
            return String.Format("{0:s}", date) + "Z";
        }

        /// <summary>
        /// Restituisce una data nel formato 2008-03-09T16:05:07
        /// </summary>
        /// <param name="date">La data da formattare</param>
        /// <returns>La data formattata</returns>
        public static string formatToSortableDateTime(DateTime date)
        {
            return String.Format("{0:s}", date);
        }

        /// <summary>
        /// Verifica se una data è nel range
        /// </summary>
        /// <param name="value"></param>
        /// <param name="dataDa"></param>
        /// <param name="dataA"></param>
        /// <returns>TRUE se è vero, false altrimenti</returns>
        public static bool inRange(DateTime value, DateTime dataDa, DateTime dataA)
        {
            return (dataDa <= value) && (value <= dataA);
        }

        public static bool inRangePce(DateTime value, DateTime dataDa)
        {
            return (dataDa <= value);
        }

        public static string FromDateTimeToString(DateTime? date)
        {
            DateTime temp = date.GetValueOrDefault();
            string dataFormat = "";

            if (date != null)
            {
                dataFormat = temp.ToString("dd/MM/yyyy HH':'mm':'ss", System.Globalization.CultureInfo.CurrentCulture);
            }

            return dataFormat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hourToRemoveByTimezone"></param>
        /// <returns></returns>
        public static string BuildStringTimeFromMidnight(int hourToRemoveByTimezone, DateTime dataSchedule)
        {
            int addDaylightSavingTime = TimeZoneInfo.Local.IsDaylightSavingTime(dataSchedule.AddDays(1)) ? 1 : 0;

            if (hourToRemoveByTimezone == 0)
            {
                return "00:00";
            }
            else
            {
                int hours = 24 - hourToRemoveByTimezone - addDaylightSavingTime;
                return hours + ":00";
            }
        }

        public static DateTime GetLastWeekdayOfMonth(DateTime date, DayOfWeek day)
        {
            DateTime lastDayOfMonth = new DateTime(date.Year, date.Month, 1)
                .AddMonths(1).AddDays(-1);
            int wantedDay = (int)day;
            int lastDay = (int)lastDayOfMonth.DayOfWeek;
            return lastDayOfMonth.AddDays(
                lastDay >= wantedDay ? wantedDay - lastDay : wantedDay - lastDay - 7);
        }

        public static DateTime GetWhenDaylightSwitchOn()
        {
            DateTime data = new DateTime(DateTime.Now.Year, 03, 01);
            return (GetLastWeekdayOfMonth(data, DayOfWeek.Sunday));
        }
        public static DateTime GetWhenDaylightSwitchOff()
        {
            DateTime data = new DateTime(DateTime.Now.Year, 10, 01);
            return (GetLastWeekdayOfMonth(data, DayOfWeek.Sunday));
        }

        public static int GetHoursPerDay(DateTime data)
        {
            int hours = 24;

            if (data.Equals(GetWhenDaylightSwitchOff())) {
                hours = 25;
            }

            if (data.Equals(GetWhenDaylightSwitchOn()))
            {
                hours = 23;
            }

            return hours;
        }
    }
}