﻿namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class MessageRepository
    {
        public const string INVALID_MAIL = "Mail non valida";
        public const string REQUIRED_FIELD = "Campo obbligatorio";
        public const string LISTA_VUOTA = "La lista non contiene elementi";
        public const string KO_OPERAZIONE = "Si è verificato un errore";
        public const string OK_OPERAZIONE = "Operazione avvenuta con successo";
        public const string NO_ATTIVITA_CHECKED = "Selezionare almeno un'attività per la ricerca";
        public const string NO_RISULTATI_RICERCA = "La ricerca non ha restituito risultati";
        public const string RICHIESTA_MODIFICA_DATA = "L’operazione eseguita ha generato una Richiesta di Modifica Data Pianificata con il seguente ID di Transazione: ";
        public const string RICHIESTA_MODIFICA_AVVISO = "L’operazione eseguita ha generato una Richiesta di Modifica Avviso con il seguente ID di Transazione: ";
        public const string RICHIESTA_MODIFICA_ORDINE = "L’operazione eseguita ha generato una Richiesta di Modifica Ordine con il seguente ID di Transazione: ";
        public const string NO_CABINA_PRESENTE = "Cabina non presente in Anagrafica"; 
    }
}