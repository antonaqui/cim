﻿using System;
using Enel.Indra.CIMBypass.Business.Common.Log;

namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class Padding
    {
        private static Log4NetLogger _Logger = new Log4NetLogger("Padding");

        /// <summary>
        /// Data una stringa (startString), il carattere da paddare (caracter) e la lunghezza totale della stringa paddata (totLenght)
        /// restituisce la stringa paddata.
        /// </summary>
        /// <param name="startString"></param>
        /// <param name="caracter"></param>
        /// <param name="totLenght"></param>
        /// <returns></returns>
        public static string PadLeftCar(string startString, char caracter, int totLenght)
        {
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, "startString: " + startString);
            string toReturn = startString;
            if (!String.IsNullOrEmpty(startString) && startString.Length < totLenght && !startString.Contains('*'))
            {
                toReturn = startString.PadLeft(totLenght, caracter);
            }
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, "paddingString: " + toReturn);
            return toReturn;
        }
    }
}