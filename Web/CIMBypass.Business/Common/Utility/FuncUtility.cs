﻿using System;

namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class FuncUtility
    {
        private const string STAR = "*";

        /// <summary>
        /// Verifica l'uguaglianza tra la stringa value1 e la stringa value2
        /// usando espressioni asteriscate
        /// ES: value1 = "PIPPO1", value2 = "PIPPO" => false
        ///     value1 = "PIPPO1", value2 = "PIPPO*" => true
        ///     value1 = "PIPPO", value2 = "PIPPO" => true
        ///     value1 = "PIPPO", value2 = "PIPPO*" => true 
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2">valore da confrontare</param>
        /// <returns></returns>
        public static bool EqualsWithStar1(this string value, string pattern)
        {
            return  string.IsNullOrEmpty(pattern) 
                    || pattern.Equals(STAR)
                    || (pattern.EndsWith(STAR) && value.StartsWith(pattern.Remove(pattern.Length - 1)))
                    ||  value.Equals(pattern);
        }


        public static readonly Func<string, string, bool> EqualsWithStar = (value, pattern) => pattern.EndsWith(STAR.ToString())
                                                                                    ? pattern.Equals(STAR) || value.StartsWith(pattern.Remove(pattern.Length - 1))
                                                                                    : value.Equals(pattern);
    }
}