﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    public static class LinqExtensions
    {
        #region Private expression tree helpers

        private static LambdaExpression GenerateSelector<TEntity>(String propertyName, out Type resultType) where TEntity : class
        {
            // Create a parameter to pass into the Lambda expression (Entity => Entity.OrderByField).
            var parameter = Expression.Parameter(typeof(TEntity), "Entity");
            //  create the selector part, but support child properties
            PropertyInfo property;
            Expression propertyAccess;
            if (propertyName.Contains('.'))
            {
                // support to be sorted on child fields.
                String[] childProperties = propertyName.Split('.');
                property = typeof(TEntity).GetProperty(childProperties[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
                for (int i = 1; i < childProperties.Length; i++)
                {
                    property = property.PropertyType.GetProperty(childProperties[i], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    propertyAccess = Expression.MakeMemberAccess(propertyAccess, property);
                }
            }
            else
            {
                property = typeof(TEntity).GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
            }
            resultType = property.PropertyType;
            // Create the order by expression.
            return Expression.Lambda(propertyAccess, parameter);
        }
        private static MethodCallExpression GenerateMethodCall<TEntity>(IQueryable<TEntity> source, string methodName, String fieldName) where TEntity : class
        {
            Type type = typeof(TEntity);
            Type selectorResultType;
            LambdaExpression selector = GenerateSelector<TEntity>(fieldName, out selectorResultType);
            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), methodName,
                            new Type[] { type, selectorResultType },
                            source.Expression, Expression.Quote(selector));
            return resultExp;
        }
        #endregion


        public static IQueryable<T> UnionIfNotEmpty<T>(this IQueryable<T> query, IQueryable<T> toAppend)
        {
            return (toAppend != null) ? query.Union(toAppend) : query;
        }

        public static Expression<Func<T, bool>> EqualsWithStar<T>(Expression<Func<T, string>> valueSelector, string pattern)
        {
            Expression<Func<T, bool>> toReturn = null;
            const string STAR = "*";

            if (string.IsNullOrEmpty(pattern) || pattern.Equals(STAR))
            {
                toReturn = s => true;
            }
            else
            {
                Expression expr;

                if (pattern.EndsWith(STAR))
                {
                    MethodInfo mi = typeof(string).GetMethod("StartsWith");
                    expr = Expression.Call(valueSelector.Body, mi, Expression.Constant(pattern.Remove(pattern.Length - 1)));
                }
                else
                {
                    expr = Expression.Equal(valueSelector.Body, Expression.Constant(pattern));
                }

                var param = valueSelector.Parameters.Single();
                toReturn = Expression.Lambda<Func<T, bool>>(expr, param);
            }

            return toReturn;
        }

        public static IOrderedQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            MethodCallExpression resultExp = GenerateMethodCall<TEntity>(source, "OrderBy", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }

        public static IOrderedQueryable<TEntity> OrderByDescending<TEntity>(this IQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            MethodCallExpression resultExp = GenerateMethodCall<TEntity>(source, "OrderByDescending", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }
        public static IOrderedQueryable<TEntity> ThenBy<TEntity>(this IOrderedQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            MethodCallExpression resultExp = GenerateMethodCall<TEntity>(source, "ThenBy", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }
        public static IOrderedQueryable<TEntity> ThenByDescending<TEntity>(this IOrderedQueryable<TEntity> source, string fieldName) where TEntity : class
        {
            MethodCallExpression resultExp = GenerateMethodCall<TEntity>(source, "ThenByDescending", fieldName);
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }
        public static IOrderedQueryable<TEntity> OrderUsingSortExpression<TEntity>(this IQueryable<TEntity> source, string sortExpression) where TEntity : class
        {
            String[] orderFields = sortExpression.Split(',');
            IOrderedQueryable<TEntity> result = null;
            for (int currentFieldIndex = 0; currentFieldIndex < orderFields.Length; currentFieldIndex++)
            {
                String[] expressionPart = orderFields[currentFieldIndex].Trim().Split(' ');
                String sortField = expressionPart[0];
                Boolean sortDescending = (expressionPart.Length == 2) && (expressionPart[1].Equals("DESC", StringComparison.OrdinalIgnoreCase));
                if (sortDescending)
                {
                    result = currentFieldIndex == 0 ? source.OrderByDescending(sortField) : result.ThenByDescending(sortField);
                }
                else
                {
                    result = currentFieldIndex == 0 ? source.OrderBy(sortField) : result.ThenBy(sortField);
                }
            }
            return result;
        }


        public static IEnumerable<T> GetParents<T>(this T component, Func<T, bool> hasParent, Func<T, T> getParent)
        {
            var stack = new Stack<T>();

            do
            {
                if (hasParent(component))
                {
                    stack.Push(getParent(component));
                }

                if (stack.Count == 0)
                    break;

                component = stack.Pop();
                yield return component;
            } while (true);
        }

        public static IEnumerable<T> GetDescendants<T>(this T component, Func<T, bool> hasChildren, Func<T, IEnumerable<T>> getChildren)
        {
            var stack = new Stack<T>();
            do
            {
                if (hasChildren(component))
                {
                    // this will currently yield the children in reverse order
                    // use "composite.Children.Reverse()" to maintain original order
                    // or let the "getChildren" delegate handle the ordering
                    foreach (var child in getChildren(component))
                    {
                        stack.Push(child);
                    }
                }

                if (stack.Count == 0)
                    break;

                component = stack.Pop();
                yield return component;
            } while (true);
        }


        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> childrenSelector)
        {
            // Do standard error checking here.

            // Cycle through all of the items.
            foreach (T item in source)
            {
                // Yield the item.
                yield return item;

                // Yield all of the children.
                foreach (T child in childrenSelector(item).Flatten(childrenSelector))
                {
                    // Yield the item.
                    yield return child;
                }
            }
        }

        /// <summary>
        /// The Entity Framework does not currently support collection-valued parameters. 
        /// To work around this restriction, one can manually construct an expression given a sequence of values 
        /// using the following utility method.
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="valueSelector"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(Expression<Func<TElement, TValue>> valueSelector, IEnumerable<TValue> values)
        {

            if (null == valueSelector) { throw new ArgumentNullException("valueSelector"); }

            if (null == values) { throw new ArgumentNullException("values"); }

            ParameterExpression p = valueSelector.Parameters.Single();

            // p => valueSelector(p) == values[0] || valueSelector(p) == ...

            if (!values.Any())
            {

                return e => false;

            }

            var equals = values.Select(value => (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));

            var body = equals.Aggregate<Expression>((accumulate, equal) => Expression.Or(accumulate, equal));

            return Expression.Lambda<Func<TElement, bool>>(body, p);

        }
    }
}
