﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Collections;
using System.Runtime.Serialization.Json;
using System.Xml;
using System.Xml.Linq;
using Enel.Indra.CIMBypass.Business.Common.Log;

namespace Enel.Indra.CIMBypass.Business.Common.Utility
{
    /// <summary>
    /// XML Serialization and Deserialization Assistant Class
    /// </summary>
    public static class XmlUtility
    {
        public static object Properties { get; private set; }

        public static string ObjectToXml<T>(T t)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            StringWriter sww = new StringWriter();
            XmlWriter writer = XmlWriter.Create(sww);
            xsSubmit.Serialize(writer, t);
            return sww.ToString();
        }
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public static string Serialize<T>(T t)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.Serialize(ms, t);
            string xmlString = Convert.ToBase64String(ms.ToArray());
            ms.Close();
            return xmlString;
        }
        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static T Deserialize<T>(string xmlString)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            Stream ms = new MemoryStream(Convert.ToByte(xmlString));
            T obj = (T)ser.Deserialize(ms);
            return obj;
        }

        /// <summary>
        /// JSON Serialization
        /// </summary>
        public static string JsonSerializer<T>(T obj)
        {
            var serializer = new DataContractJsonSerializer(obj.GetType());
            var ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            return Encoding.Default.GetString(ms.ToArray());
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }


        public static string GetQueryString(object request, string separator = ",")
        {
            if (request == null)
                throw new ArgumentNullException("request");

            // Get all properties on the object
            var properties = request.GetType().GetProperties()
                .Where(x => x.CanRead)
                .Where(x => x.GetValue(request, null) != null)
                .ToDictionary(x => x.Name, x => x.GetValue(request, null));

            // Get names for all IEnumerable properties (excl. string)
            var propertyNames = properties
                .Where(x => !(x.Value is string) && x.Value is IEnumerable)
                .Select(x => x.Key)
                .ToList();

            // Concat all IEnumerable properties into a comma separated string
            foreach (var key in propertyNames)
            {
                var valueType = properties[key].GetType();
                var valueElemType = valueType.IsGenericType
                                        ? valueType.GetGenericArguments()[0]
                                        : valueType.GetElementType();
                if (valueElemType.IsPrimitive || valueElemType == typeof(string))
                {
                    var enumerable = properties[key] as IEnumerable;
                    properties[key] = string.Join(separator, enumerable.Cast<object>());
                }
            }

            // Concat all key/value pairs into a string separated by ampersand
            return string.Join("&", properties
                .Select(x => string.Concat(
                    Uri.EscapeDataString(x.Key), "=",
                    Uri.EscapeDataString(x.Value.ToString()))));
        }



        public static String prettifyXML(String XML)
        {
            Log4NetLogger _Logger;
            _Logger = new Log4NetLogger("XmlUtility");
            _Logger.DebugIn(nameof(prettifyXML), "[Input: " + XML + "]");

            String Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(XML);

                writer.Formatting = Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                String FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException e)
            {
                _Logger.Fatal(nameof(prettifyXML), e);
                throw;
            }

            mStream.Close();
            writer.Close();

            _Logger.DebugOut(nameof(prettifyXML), "[Output: " + XML + "]");

            return Result;
        }

        internal static string cleanXmlns(string testoFinaleXML)
        {
            XDocument doc = XDocument.Parse(testoFinaleXML);
            // All elements with an empty namespace...
            foreach (var node in doc.Root.Descendants()
                                    .Where(n => n.Name.NamespaceName != ""))
            {
                node.Attributes("xmlns").Remove();
                // Inherit the parent namespace instead
                node.Name = node.Parent.Name.Namespace + node.Name.LocalName;
            }

            return doc.ToString();
        }
    }
}