﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Enel.Indra.Smart.Business.Common.Utility
{
    public static class StringUtility
    {
        public static string concatList(List<string> lista)
        {

            string toReturn = "";

            foreach (string temp in lista)
            {
                toReturn = String.Concat(toReturn, temp);

            }
            return toReturn;
        }

        public static string applicaRegolaData(string separatoreIniziale, string separatoreFinale, DateTime unaData, string namingFile)
        {
            string pattern = Regex.Escape(separatoreIniziale) + "\\w+" + Regex.Escape(separatoreFinale);
            MatchCollection matchList = Regex.Matches(namingFile, pattern);
            if (matchList.Count > 0)
            {
                IEnumerator @enum = matchList.GetEnumerator();
                @enum.MoveNext();
                string formatoData = @enum.Current.ToString().Replace(separatoreFinale, "").Replace(separatoreIniziale, "");

                try
                {
                    namingFile = namingFile.Replace(separatoreIniziale + formatoData + separatoreFinale, unaData.ToString(formatoData));
                }
                catch
                {
                    namingFile = namingFile.Replace(separatoreIniziale + formatoData + separatoreFinale, "");
                }
            }

            return namingFile;
        }
    }
}