﻿using System;
using log4net;
using System.Collections;

namespace Enel.Indra.CIMBypass.Business.Common.Log
{
    public class Log4NetLogger
    {
        private readonly ILog _logger;
        private readonly string _name;
        public string tid { get; set; }

        
        public Log4NetLogger(string classname)
        {
            _name = classname;
            _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().GetType());
        }

     
        public void Info(string methodName, string message)
        {
            _logger.Info(composeMethodAndDesc(methodName, message));
        }

  
        public void Warn(string methodName, string message)
        {
            _logger.Warn(composeMethodAndDesc(methodName, message));
        }

        public void Warn(string methodName, Exception x)
        {
            _logger.Warn(composeMethodAndDesc(methodName, x.StackTrace));
        }

        public void Debug(string methodName, string desc)
        {
            _logger.Debug(composeMethodAndDesc(methodName, desc));
        }

        public void DebugIn(string methodName, string parameters)
        {
            composeLog("IN", methodName, parameters);
        }

    
        public void DebugOut(string methodName, string parameters)
        {
            composeLog("OUT", methodName, parameters);
        }

        public void DebugOut(string methodName, object item)
        {
            composeLog("OUT", methodName, "found: " + (item != null).ToString());
        }

        public void DebugOut(string methodName, IList list)
        {
            composeLog("OUT", methodName, "count: " + list.Count);
        }

        private void composeLog(string direction, string methodName, string parameters)
        {
            string message = string.Format(_name + ".{0}(" + composeTid() + ") - {1} {2}", methodName, direction, parameters);
            _logger.Debug(message);
        }

        private string composeMethodAndDesc(string methodName, string parameters)
        {
            string message = string.Format(_name + ".{0}(" + composeTid() + ") - {1}", methodName, parameters);
            return message;
        }

        public void Error(string methodName, string message)
        {
            _logger.Error(composeMethodAndDesc(methodName, message));
        }

        public void Error(string message, Exception x)
        {
            _logger.Error(message, x);
        }

        public void Error(Exception x)
        {
            _logger.Error(x);
        }

        public void Fatal(string methodName, string message)
        {
            _logger.Fatal(composeMethodAndDesc(methodName, message));
        }

        public void Fatal(string methodName, Exception x)
        {
            _logger.Fatal(methodName, x);
        }

        private string composeTid()
        {
            if (!String.IsNullOrEmpty(tid))
                return "[" + tid + "]";
            else
                return "";
        }

       
    }
}

