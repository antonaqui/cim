﻿using System;
using System.Collections;

namespace Enel.Indra.CIMBypass.Business.Common.Log
{
    public interface ILog4netLogger<T> where T : class
    {

        void Info(string methodName, string message);
        
        void Warn(string methodName, string message);

        void Warn(string methodName, Exception x);

        void Debug(string methodName, string desc);

        void DebugIn(string methodName, string parameters);

        void DebugOut(string methodName, string parameters);

        void DebugOut(string methodName, object item);

        void DebugOut(string methodName, IList list);
       
        void Error(string methodName, string message);

        void Error(string message, Exception x);

        void Error(Exception x);

        void Fatal(string methodName, string message);

        void Fatal(string methodName, Exception x);

 
    }
}
