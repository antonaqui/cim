﻿using System;
using System.Data;
using System.Data.Common;

namespace Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework
{
    public interface IDbConnector
    {
        int ExecuteNonStoredProcedureOrQuery(CommandType type, String commandText, params DbParameter[] parameter);

        DataTable ExecuteStoredProcedureOrQuery(CommandType type, String commandText, params DbParameter[] parameter);


        int GetSequenceNextVal(string sequenceName);

    }
}