﻿using System;
using System.Data.Common;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Microsoft.Extensions.Configuration;
using System.IO;
using Enel.Indra.CIMBypass.Business.Common.Log;

namespace Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework
{
    public class OracleDbConnector : DbConnector
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("OracleDbConnector");

        protected override DbCommand InstanceCommand(CommandType type, String commandText)
        {
            _Logger.DebugIn(nameof(InstanceCommand), "CommandType: " + type.ToString() + ", commandText: " + commandText);

            OracleCommand comando = new OracleCommand();

            comando.CommandType = type;
            comando.CommandText = commandText;
            comando.Connection = (OracleConnection) GetConnection();

            _Logger.DebugOut(nameof(InstanceCommand), "comando.CommandText: " + comando.CommandText);

            return comando;
        }

        protected override DbConnection GetConnection()
        {
            _Logger.DebugIn(nameof(GetConnection), "NO PARAMETER");

            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", false, true).AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: true).Build();

            var connectionString = configuration.GetValue<string>("ConnectionString:CimConnection");

            if (connectionString != null)
                _connection = new OracleConnection(connectionString);

            _Logger.DebugOut(nameof(GetConnection), "_connection: " + _connection.ToString());

            return _connection;
        }

        public override int GetSequenceNextVal(string sequenceName)
        {
            return -1;
        }

        protected override DbCommand InstanceCommand(DbConnection connection, CommandType type, string commandText)
        {
            _Logger.DebugIn(nameof(InstanceCommand), "CommandType: " + type.ToString() + ", commandText: " + commandText);

            OracleCommand comando = new OracleCommand();

            comando.CommandType = type;
            comando.CommandText = commandText;
            comando.Connection = (OracleConnection) connection;
            OpenConnection(connection);

            _Logger.DebugOut(nameof(InstanceCommand), "comando.CommandText: " + comando.CommandText);

            return comando;
        }
    }
}
