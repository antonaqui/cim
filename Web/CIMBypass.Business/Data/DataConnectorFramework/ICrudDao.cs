﻿using System.Collections.Generic;

namespace Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework
{
    /// <summary>
    /// @author: Antonio Aquino
    /// @version: 1.0
    /// @data: 2019-03-01
    /// @description: Interfaccia generica per eseguire CRUD
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    public interface ICrudDao<Type> where Type : class
    {
        /// <summary>
        /// Crea l'oggetto in input che sarà valorizzato in output con la nuova chiave
        /// </summary>
        /// <param name="source">oggetto da creare</param>
        /// <returns>numero di oggetti creati</returns>
        int Create(Type source);

        /// <summary>
        /// Esegue una retrieve all
        /// </summary>
        /// <returns>lista degli oggetti in uscita</returns>
        List<Type> Retrieve();

        /// <summary>
        /// Esegue una retrieve by id. 0 or 1 object type is returned
        /// </summary>
        /// <returns></returns>
        Type GetById(Type source);

        /// <summary>
        /// Esegue un update dell'oggetto di tutti i parametri di input
        /// </summary>
        /// <param name="source">nuovi valori dell'oggetto da aggiornare</param>
        /// <returns></returns>
        int Update(Type source);

        /// <summary>
        /// Esegue una delete parametrizzata in base al filtro
        /// </summary>
        /// <param name="source">oggetto da cancellare</param>
        /// <returns>numero di oggetti cancellati</returns>
        int Delete(Type source);
    }
}
