﻿using Enel.Indra.CIMBypass.Business.Common.Log;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework
{
    public abstract class DbConnector : IDbConnector
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("DbConnector");

        protected DbConnection _connection;

        protected void OpenConnection()
        {
            _Logger.DebugIn(nameof(OpenConnection), "NO PARAMETER");

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            _Logger.DebugOut(nameof(OpenConnection), "NO OUTUPUT");
        }

        protected void OpenConnection(DbConnection _connection)
        {
            _Logger.DebugIn(nameof(OpenConnection), "_connection");

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            _Logger.DebugOut(nameof(OpenConnection), "NO OUTUPUT");
        }

        protected void CloseConnection()
        {
            _Logger.DebugIn(nameof(CloseConnection), "NO PARAMETER");
            if (_connection != null)
            {
                if (_connection.State != ConnectionState.Closed)
                    _connection.Close();
            }
            _Logger.DebugOut(nameof(CloseConnection), "NO OUTUPUT");
        }

        protected void CloseConnection(DbConnection _connection)
        {
            _Logger.DebugIn(nameof(CloseConnection), "_connection");
            if (_connection != null)
            {
                if (_connection.State != ConnectionState.Closed)
                    _connection.Close();
            }
            _Logger.DebugOut(nameof(CloseConnection), "NO OUTUPUT");
        }

        protected static void AddParameter(DbCommand cmd, params DbParameter[] param)
        {
            if (param != null)
            {
                foreach (DbParameter p in param)
                {
                    p.Value = p.Value ?? DBNull.Value;
                    cmd.Parameters.Add(p);
                }
            }
        }

        public void Dispose()
        {
            _Logger.DebugIn(nameof(Dispose), "NO PARAMETER");
            if (_connection != null)
            {
                CloseConnection();
                _connection.Dispose();
            }
            _Logger.DebugOut(nameof(Dispose), "NO OUTUPUT");
        }

        public int ExecuteNonStoredProcedureOrQuery(CommandType type, String commandText, params DbParameter[] parameter)
        {
            _Logger.DebugIn(nameof(ExecuteNonStoredProcedureOrQuery), "type" + type + ", commandText: " + commandText + ", parameter.Length: " + parameter.Length);
            int toReturn = 0;
            DbCommand cmd = null;
            try
            {
                cmd = InstanceCommand(type, commandText);
                AddParameter(cmd, parameter);
                OpenConnection();
                toReturn = cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
                cmd = null;

                CloseConnection();
            }

            _Logger.DebugOut(nameof(ExecuteNonStoredProcedureOrQuery), "toReturn: " + toReturn);

            return toReturn;
        }

        public DataTable ExecuteStoredProcedureOrQuery(CommandType type, String commandText, params DbParameter[] parameter)
        {
            _Logger.DebugIn(nameof(ExecuteStoredProcedureOrQuery), "type: " + type + ", commandText: " + commandText + ", parameter.Length: " + parameter.Length);

            DbCommand cmd = null;
            DbDataReader reader = null;
            DataTable toReturn = null;
            try
            {
                cmd = InstanceCommand(type, commandText);
                AddParameter(cmd, parameter);
                OpenConnection();
                reader = cmd.ExecuteReader();

                if (reader != null)
                {
                    toReturn = new DataTable();
                    toReturn.Load(reader);
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                CloseConnection();
            }

            _Logger.DebugOut(nameof(ExecuteStoredProcedureOrQuery), "toReturn: " + toReturn);
            return toReturn;
        }

        public IEnumerable<T> ExecuteStoredProcedureOrQueryGeneric<T>(CommandType type, String commandText, Func<IDataRecord, T> BuildObject, params DbParameter[] parameter)
        {
            _Logger.DebugIn(nameof(ExecuteStoredProcedureOrQueryGeneric), "type: " + type + ", commandText: " + commandText + ", parameter.Length: " + parameter.Length);

            IEnumerable<T> lista = new List<T>();
            OracleConnection con = null;
            DbCommand cmdOracle = null;
            OracleDataReader reader = null;

            try
            {
                using (con = (OracleConnection) GetConnection())
                {
                    using (cmdOracle = (OracleCommand) InstanceCommand(con, type, commandText)) {
                        AddParameter(cmdOracle, parameter);

                        reader = (OracleDataReader) cmdOracle.ExecuteReader();

                        if (reader != null)
                        {
                            lista = GetData(reader, BuildObject);
                        }
                    }
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmdOracle != null)
                    cmdOracle.Dispose();

                CloseConnection(con);
            }

            _Logger.DebugOut(nameof(ExecuteStoredProcedureOrQueryGeneric), "Lista: " + lista.Count());
            return lista;
        }

        public object ExecuteScalar(CommandType type, String commandText, params DbParameter[] parameter)
        {
            _Logger.DebugIn(nameof(ExecuteScalar), "type: " + type + ", commandText: " + commandText + ", parameter.Length: " + parameter.Length);

            DbCommand cmd = null;
            object toReturn = null;
            try
            {
                cmd = InstanceCommand(type, commandText);
                AddParameter(cmd, parameter);
                OpenConnection();
                toReturn = cmd.ExecuteScalar();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();

                CloseConnection();
            }

            _Logger.DebugOut(nameof(ExecuteScalar), "toReturn: " + toReturn);
            return toReturn;
        }

        public List<T> GetData<T>(IDataReader reader, Func<IDataRecord, T> BuildObject)
        {
            _Logger.DebugIn(nameof(GetData), "BuildObject: " + BuildObject.ToString());

            List<T> lista = new List<T>();
            try
            {
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        T record = BuildObject(reader);
                        lista.Add(record);
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(GetData), ex);
                throw ex;
            }

            _Logger.DebugOut(nameof(GetData), "lista.Count: " + lista.Count);
            return lista;
        }

        protected abstract DbConnection GetConnection();

        protected abstract DbCommand InstanceCommand(CommandType type, String commandText);

        protected abstract DbCommand InstanceCommand(DbConnection connection, CommandType type, String commandText);

        public abstract int GetSequenceNextVal(string sequenceName);

    }
}
