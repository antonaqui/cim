﻿using Enel.Indra.CIMBypass.Business.Data.Dao;
using Enel.Indra.CIMBypass.Business.Service;
using Microsoft.Extensions.DependencyInjection;


namespace Enel.Indra.CIMBypass.Business.Data
{
    public static class DependencyActivator
    {
        public static IServiceCollection Activate(IServiceCollection services)
        {
            services.AddSingleton<IUploadService, UploadService>();
            services.AddSingleton<IActionService, ActionService>();
            services.AddSingleton<IArchivioFileDao, ArchivioFileDao>();
            services.AddSingleton<ICatFlussiDao, CatFlussiDao>();
            services.AddSingleton<IArchivioFileCurvePegDao, ArchivioFileCurvePegDao>();
            return services;
        }
    }
}
