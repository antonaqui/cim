﻿namespace Enel.Indra.CIMBypass.Business.Data.Model.Enumerator
{
    public class EnumOperationMessage
    {
        private EnumOperationMessage() { }
        public static readonly string OP_SUCCESS = "Operazione effettuata con successo.";
        public static readonly string OP_ERROR = "Si è verificato un errore.";
        public static readonly string OP_WARNING = "Input non corretti.";
        public static readonly string OP_OUTOFRANGE = "La ricerca ha restituito troppi risultati.";
        public static readonly string FILE_SUCCESS = "File elaborato con successo.";
        public static readonly string FILE_SUCCESS_SCARTI = "File elaborato con successo, ma con righe scartate.";
        public static readonly string FILE_ERROR  = "Si è verificato un errore nell'elaborazione del file.";
        public static readonly string FILE_ERROR_TEMPLATE = "Il template non è configurato.";
    }
}