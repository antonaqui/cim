﻿using System.ComponentModel;

namespace Enel.Indra.CIMBypass.Business.Data.Model.Enumerator
{
    public enum EnumSiNo
    {
        [Description("S")]
        S = 1,
        [Description("N")]
        N = 2,
        [Description("Si")]
        Si = 3,
        [Description("No")]
        No = 4,
    }
}