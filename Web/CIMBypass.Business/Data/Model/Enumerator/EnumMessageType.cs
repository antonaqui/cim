﻿namespace Enel.Indra.CIMBypass.Business.Data.Model.Enumerator
{
    public enum EnumMessageType
    {
        None = 0,
        Information = 1,
        Warning = 2,
        Error = 3,
        Success = 4,
    }
}
