﻿namespace Enel.Indra.CIMBypass.Business.Data.Model.Enumerator
{
    public class EnumIcon
    {
        private EnumIcon() { }

        public static readonly string ICON_SUCCESS = "thumbs-up";
        public static readonly string ICON_ERROR = "thumbs-down";
        public static readonly string ICON_WARNING = "exclamation-triangle";
        public static readonly string ICON_OUTOFRANGE = "exclamation-triangle";
        public static readonly string ICON_INFORMATION = "info";
    }
}