﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enel.Indra.CIMBypass.Business.Data.Model.Entity
{
    public class ArchivioFileCurvePegModel
    {

        public ArchivioFileCurvePegModel() { }

        public Decimal IdArchivioFileCurvePeg { get; set; }
        public Decimal idArchivioFile { get; set; }
        public string Skid { get; set; }
        public string Sknum { get; set; }
        public string Terid { get; set; }
        public string Sezid { get; set; }
        public string Comba { get; set; }
        public string Caloa0 { get; set; }
        public string Caloa1 { get; set; }
        public string Caloa2 { get; set; }
        public string Combb { get; set; }
        public string Calob0 { get; set; }
        public string Calob1 { get; set; }
        public string Calob2 { get; set; }
        public string Combc { get; set; }
        public string Caloc0 { get; set; }
        public string Caloc1 { get; set; }
        public string Caloc2 { get; set; }
        public string Areid { get; set; }
        public string Cocmba { get; set; }
        public string Cocmbb { get; set; }
        public string Cocmbc { get; set; }
        public string Cfcmba { get; set; }
        public string Cfcmbb { get; set; }
        public string Cfcmbc { get; set; }
    }
}
