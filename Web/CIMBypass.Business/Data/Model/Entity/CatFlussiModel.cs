﻿using Newtonsoft.Json;
using System;
using System.Data;

namespace Enel.Indra.CIMBypass.Business.Data.Model.Entity
{
    public class CatFlussiModel : IDBMap<CatFlussiModel>
    {
        public CatFlussiModel()
        {
        }

        public Decimal IdFlusso { get; set; }
        public string NomeFlusso { get; set; }
        public DateTime DataConfigurazione { get; set; }
        public Decimal IdUtenteInserimento { get; set; }
        public string Attivo { get; set; }
        public string HasPeriod { get; set; }
        public string TargetTable { get; set; }
        public string NamingConventionFile { get; set; }
        public string ClasseRuntime { get; set; }

        public new static CatFlussiModel RecordToModel(IDataRecord record)
        {
            return new CatFlussiModel
            {
                IdFlusso = (Decimal)record["id_flusso"],
                NomeFlusso = (string)record["nome_flusso"],
                DataConfigurazione = (DateTime)record["data_configurazione"],
                IdUtenteInserimento = (Decimal)record["id_utente_inserimento"],
                Attivo = (string)record["attivo"],
                HasPeriod = (string)record["has_period"],
                TargetTable = (string)record["target_table"],
                NamingConventionFile = (string)record["naming_convention_file"],
                ClasseRuntime = (string)record["classe_runtime"]
            };
        }
    }
}
