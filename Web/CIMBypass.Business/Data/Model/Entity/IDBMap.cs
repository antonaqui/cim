﻿using Newtonsoft.Json;
using System;
using System.Data;

namespace Enel.Indra.CIMBypass.Business.Data.Model.Entity
{
    public abstract class IDBMap<T> where T : class
    {

        public static T RecordToModel(IDataRecord record) {
            T obj = null;
            return obj;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
