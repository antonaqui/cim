﻿using System;
using System.Data;

namespace Enel.Indra.CIMBypass.Business.Data.Model.Entity
{
    public class ArchivioFileModel : IDBMap<ArchivioFileModel>
    {
        public ArchivioFileModel()
        {
        }

        public Decimal IdArchivio { get; set; }
        public string NomeFile { get; set; }
        public string MimeFile { get; set; }
        public byte[] ByteFile { get; set; }
        public DateTime DataCaricamento { get; set; }
        public Decimal IdFlusso { get; set; }
        public DateTime DataFlussoDa { get; set; }
        public DateTime DataFlussoA { get; set; }
        public Decimal IdUtenteCaricamento { get; set; }
        public string Elaborato { get; set; }
        public DateTime? DataElaborazione { get; set; }
        public Decimal? IdUtenteElaborazione { get; set; }
        public new static ArchivioFileModel RecordToModel(IDataRecord record)
        {
            return new ArchivioFileModel
            {
                IdArchivio = (Decimal)record["id_archivio"],
                NomeFile = (string)record["nome_file"],
                MimeFile = (string)record["mime_file"],
                ByteFile = (byte[])record["byte"],
                DataCaricamento = (DateTime)record["date_caricamento"],
                IdFlusso = (Decimal)record["id_flusso"],
                DataFlussoDa = (DateTime)record["date_flusso_da"],
                DataFlussoA = (DateTime)record["date_flusso_da"],
                IdUtenteCaricamento = (Decimal)record["id_utente_caricamento"],
                Elaborato = (string)record["elaborato"],
                DataElaborazione = (DateTime?)((record["date_elaborazione"] == DBNull.Value) ? null : record["date_elaborazione"]),
                IdUtenteElaborazione = (Decimal?)((record["id_utente_elaborazione"] == DBNull.Value) ? null : record["id_utente_elaborazione"])

            };
        }

        public static ArchivioFileModel RecordToModelNoByte(IDataRecord record)
        {
            return new ArchivioFileModel
            {
                IdArchivio = (Decimal)record["id_archivio"],
                NomeFile = (string)record["nome_file"],
                MimeFile = (string)record["mime_file"],
                DataCaricamento = (DateTime)record["date_caricamento"],
                IdFlusso = (Decimal)record["id_flusso"],
                DataFlussoDa = (DateTime)record["date_flusso_da"],
                DataFlussoA = (DateTime)record["date_flusso_da"],
                IdUtenteCaricamento = (Decimal)record["id_utente_caricamento"],
                Elaborato = (string)record["elaborato"],
                DataElaborazione = (DateTime?)((record["date_elaborazione"] == DBNull.Value) ? null : record["date_elaborazione"]),
                IdUtenteElaborazione = (Decimal?)((record["id_utente_elaborazione"] == DBNull.Value) ? null : record["id_utente_elaborazione"])
            };
        }
    }
}
