﻿namespace Enel.Indra.CIMBypass.Business.Data.Model.CustomModel
{
    /// <summary>
    /// Classe che astrae un file
    /// </summary>
    public class CustomFileModel
    {
        public CustomFileModel() { }

        public CustomFileModel(string name, byte[] contenuto, string mimeType)
        {
            this.nome = name;
            this.contenuto = contenuto;
            this.mimeType = mimeType;
        }

        public string nome { get; set; }
        public byte[] contenuto { get; set;}
        public string mimeType { get; set; }
    }
}