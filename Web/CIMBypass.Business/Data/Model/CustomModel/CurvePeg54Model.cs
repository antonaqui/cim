﻿using Enel.Indra.CIMBypass.Business.Data.Model.Entity;

namespace Enel.Indra.CIMBypass.Business.Data.Model.CustomModel
{
    public class CurvePeg54Model
    {
        public string Skid { get; set; }
        public string Sknum { get; set; }
        public string Terid { get; set; }
        public string Sezid { get; set; }
        public string Comba { get; set; }
        public string Caloa0 { get; set; }
        public string Caloa1 { get; set; }
        public string Caloa2 { get; set; }
        public string Combb { get; set; }
        public string Calob0 { get; set; }
        public string Calob1 { get; set; }
        public string Calob2 { get; set; }
        public string Combc { get; set; }
        public string Caloc0 { get; set; }
        public string Caloc1 { get; set; }
        public string Caloc2 { get; set; }
        public string Areid { get; set; }

        public CurvePeg54Model(string linea)
        {
            Skid = GetSkid(linea);
            Sknum = GetSknum(linea);
            Terid = GetTerid(linea);
            Sezid = GetSezid(linea);
            Comba = GetComba(linea);
            Caloa0 = GetCaloa0(linea);
            Caloa1 = GetCaloa1(linea);
            Caloa2 = GetCaloa2(linea);
            Combb = GetCombb(linea);
            Calob0 = GetCalob0(linea);
            Calob1 = GetCalob1(linea);
            Calob2 = GetCalob2(linea);
            Combc = GetCombc(linea);
            Caloc0 = GetCaloc0(linea);
            Caloc1 = GetCaloc1(linea);
            Caloc2 = GetCaloc2(linea);
            Areid = GetAreid(linea);
        }

        private string GetSkid(string linea) { var skid = linea.Substring(0, 4); return skid; }
        private string GetSknum(string linea) { var sknum = linea.Substring(4, 2); return sknum; }
        private string GetTerid(string linea) { var colonna = linea.Substring(6, 8); return colonna; }
        private string GetSezid(string linea) { var colonna = linea.Substring(14, 8); return colonna; }
        private string GetComba(string linea) { var colonna = linea.Substring(23, 1); return colonna; }
        private string GetCaloa0(string linea) { var colonna = linea.Substring(24, 6); return colonna; }
        private string GetCaloa1(string linea) { var colonna = linea.Substring(30, 6); return colonna; }
        private string GetCaloa2(string linea) { var colonna = linea.Substring(36, 6); return colonna; }
        private string GetCombb(string linea) { var colonna = linea.Substring(42, 1); return colonna; }
        private string GetCalob0(string linea) { var colonna = linea.Substring(43, 6); return colonna; }
        private string GetCalob1(string linea) { var colonna = linea.Substring(49, 6); return colonna; }
        private string GetCalob2(string linea) { var colonna = linea.Substring(55, 6); return colonna; }
        private string GetCombc(string linea) { var colonna = linea.Substring(61, 1); return colonna; }
        private string GetCaloc0(string linea) { var colonna = linea.Substring(62, 6); return colonna; }
        private string GetCaloc1(string linea) { var colonna = linea.Substring(68, 6); return colonna; }
        private string GetCaloc2(string linea) { var colonna = linea.Substring(74, 6); return colonna; }
        private string GetAreid(string linea) { string colonna; try { colonna = linea.Substring(80, 8); } catch { return ""; } return colonna; }

        public ArchivioFileCurvePegModel ConvertiInArchivioFileCurvePegModel()
        {
            ArchivioFileCurvePegModel convertito = new ArchivioFileCurvePegModel();

            convertito.Skid = this.Skid;
            convertito.Sknum = this.Sknum;
            convertito.Terid = this.Terid;
            convertito.Sezid = this.Sezid;
            convertito.Comba = this.Comba;
            convertito.Caloa0 = this.Caloa0;
            convertito.Caloa1 = this.Caloa1;
            convertito.Caloa2 = this.Caloa2;
            convertito.Combb = this.Combb;
            convertito.Calob0 = this.Calob0;
            convertito.Calob1 = this.Calob1;
            convertito.Calob2 = this.Calob2;
            convertito.Combc = this.Combc;
            convertito.Caloc0 = this.Caloc0;
            convertito.Caloc1 = this.Caloc1;
            convertito.Caloc2 = this.Caloc2;
            convertito.Areid = this.Areid;

            return convertito;
        }
    }
}
