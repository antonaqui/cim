﻿using Enel.Indra.CIMBypass.Business.Data.Model.Enumerator;


namespace Enel.Indra.CIMBypass.Business.Data.Model.CustomModel
{
     public class OperationResultModel
    {
        public string Message { get; set; }
        public EnumMessageType Type { get; set; }
        public string IconName { get; set; }

        public OperationResultModel()
        {

        }

    }
}
