﻿using Enel.Indra.CIMBypass.Business.Data.Model.Entity;

namespace Enel.Indra.CIMBypass.Business.Data.Model.CustomModel
{
    public class CurvePeg56Model 
    {
        public string Skid { get; set; }
        public string Sknum { get; set; }
        public string Terid { get; set; }
        public string Sezid { get; set; }
        public string Comba { get; set; }
        public string Combb { get; set; }
        public string Combc { get; set; }
        public string Cocmba { get; set; }
        public string Cocmbb { get; set; }
        public string Cocmbc { get; set; }
        public string Cfcmba { get; set; }
        public string Cfcmbb { get; set; }
        public string Cfcmbc { get; set; }
        public string Areid { get; set; }

        public CurvePeg56Model(string linea) {
            Skid = GetSkid(linea);
            Sknum = GetSknum(linea);
            Terid = GetTerid(linea);
            Sezid = GetSezid(linea);
            Comba = GetComba(linea);
            Combb = GetCombb(linea);
            Combc = GetCombc(linea);
            Cocmba = GetCocmba(linea);
            Cocmbb = GetCocmbb(linea);
            Cocmbc = GetCocmbc(linea);
            Cfcmba = GetCfcmba(linea);
            Cfcmbb = GetCfcmbb(linea);
            Cfcmbc = GetCfcmbc(linea);
            Areid = GetAreid(linea);
        }

        private string GetSkid(string linea) { var skid = linea.Substring(0, 4); return skid; }
        private string GetSknum(string linea) { var sknum = linea.Substring(4, 2); return sknum; }
        private string GetTerid(string linea) { var colonna = linea.Substring(6, 2); return colonna; }
        private string GetSezid(string linea) { var colonna = linea.Substring(14, 2); return colonna; }
        private string GetComba(string linea) { var colonna = linea.Substring(22, 2); return colonna; }
        private string GetCocmba(string linea) { var colonna = linea.Substring(23, 2); return colonna; }
        private string GetCombb(string linea) { var colonna = linea.Substring(32, 2); return colonna; }
        private string GetCocmbb(string linea) { var colonna = linea.Substring(33, 2); return colonna; }
        private string GetCombc(string linea) { var colonna = linea.Substring(42, 2); return colonna; }
        private string GetCocmbc(string linea) { var colonna = linea.Substring(43, 2); return colonna; }
        private string GetCfcmba(string linea) { var colonna = linea.Substring(53, 2); return colonna; }
        private string GetCfcmbb(string linea) { var colonna = linea.Substring(63, 2); return colonna; }
        private string GetCfcmbc(string linea) { var colonna = linea.Substring(73, 7); return colonna; }
        private string GetAreid(string linea) { string colonna; try { colonna = linea.Substring(80, 8); } catch { return ""; } return colonna; }
        public ArchivioFileCurvePegModel ConvertiInArchivioFileCurvePegModel() {
            ArchivioFileCurvePegModel convertito = new ArchivioFileCurvePegModel();

            convertito.Skid = this.Skid;
            convertito.Sknum = this.Sknum;
            convertito.Terid = this.Terid;
            convertito.Sezid = this.Sezid;
            convertito.Comba = this.Comba;
            convertito.Combb = this.Combb;
            convertito.Combc = this.Combc;
            convertito.Cocmba = this.Cocmba;
            convertito.Cocmbb = this.Cocmbb;
            convertito.Cocmbc = this.Cocmbc;
            convertito.Cfcmba = this.Cfcmba;
            convertito.Cfcmbb = this.Cfcmbb;
            convertito.Cfcmbc = this.Cfcmbc;
            convertito.Areid = this.Areid;

            return convertito;
        }
    }
}
