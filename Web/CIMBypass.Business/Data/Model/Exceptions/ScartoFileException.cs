﻿using System;
using System.Runtime.Serialization;

namespace Enel.Indra.CIMBypass.Business.Data.Model.Exceptions
{
    [Serializable]
    public class ScartoFileException : Exception
    {
        public ScartoFileException()
        {
        }

        public ScartoFileException(string message) : base(message)
        {
        }

        public ScartoFileException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ScartoFileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}