﻿using Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;

namespace Enel.Indra.CIMBypass.Business.Data.Dao
{
    public interface IArchivioFileCurvePegDao : ICrudDao<ArchivioFileCurvePegModel>
    {
    }
}
