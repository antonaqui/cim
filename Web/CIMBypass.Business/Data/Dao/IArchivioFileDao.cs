﻿using Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using System.Collections.Generic;

namespace Enel.Indra.CIMBypass.Business.Data.Dao
{
    public interface IArchivioFileDao : ICrudDao<ArchivioFileModel>
    {
        List<ArchivioFileModel> ElencoFileDaElaborare();
        List<ArchivioFileModel> ElencoFileElaborati();
        bool ElaboraFile(int idCaricamento);
    }
}
