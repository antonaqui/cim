﻿using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Enel.Indra.CIMBypass.Business.Data.Dao
{
    public class ArchivioFileDao : OracleDbConnector, IArchivioFileDao
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("ArchivioFileDao");

        public int Create(ArchivioFileModel source)
        {
            _Logger.DebugIn(nameof(Create), "source" + source.ToString());

            int inserito = 0;

            string query = @"INSERT INTO CB_ARCHIVIO_FILE(nome_file, mime_file, byte, id_flusso , date_flusso_da,
                             date_flusso_a, id_utente_caricamento)VALUES(:nome_file, :mime_file,
                             :byte, :id_flusso, :date_flusso_da, :date_flusso_a, :id_utente_caricamento)";

            try
            {
                var parameters = new[]{
                    new OracleParameter(){ ParameterName = "nome_file", Value = source.NomeFile },
                    new OracleParameter(){ ParameterName = "mime_file", Value = source.MimeFile },
                    new OracleParameter(){ ParameterName = "byte", Value = source.ByteFile, OracleDbType = OracleDbType.Blob },
                    new OracleParameter(){ ParameterName = "id_flusso", Value = source.IdFlusso },
                    new OracleParameter(){ ParameterName = "date_flusso_da", Value = source.DataFlussoDa },
                    new OracleParameter(){ ParameterName = "date_flusso_a", Value = source.DataFlussoDa },
                    new OracleParameter(){ ParameterName = "id_utente_caricamento", Value = source.IdUtenteCaricamento },
                };

                inserito = ExecuteNonStoredProcedureOrQuery(CommandType.Text, query, parameters);
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(Create), ex);
                throw ex;
            }

            _Logger.DebugOut(nameof(Create), "inserito: " + inserito);

            return inserito;
        }

        public int Delete(ArchivioFileModel source)
        {
            throw new NotImplementedException();
        }

        public bool ElaboraFile(int idCaricamento)
        {
            throw new NotImplementedException();
        }

        public List<ArchivioFileModel> ElencoFileDaElaborare()
        {
            _Logger.DebugIn(nameof(ElencoFileDaElaborare), "NO PARAMETER");

            List<ArchivioFileModel> elencoDaElaborare = new List<ArchivioFileModel>();
            string query = @"SELECT * FROM CB_ARCHIVIO_FILE WHERE ELABORATO = 'N'";

            try
            {
                elencoDaElaborare = (List<ArchivioFileModel>)ExecuteStoredProcedureOrQueryGeneric(CommandType.Text, query, ArchivioFileModel.RecordToModelNoByte);
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(ElencoFileDaElaborare), ex);
                throw ex;
            }

            _Logger.DebugOut(nameof(ElencoFileDaElaborare), "elencoDaElaborare.Count: " + elencoDaElaborare.Count);

            return elencoDaElaborare;
        }

        public List<ArchivioFileModel> ElencoFileElaborati()
        {
            _Logger.DebugIn(nameof(ElencoFileElaborati), "NO PARAMETER");

            List<ArchivioFileModel> elencoElaborati = new List<ArchivioFileModel>();
            string query = @"SELECT * FROM CB_ARCHIVIO_FILE WHERE ELABORATO != 'N'";

            try
            {
                elencoElaborati = (List<ArchivioFileModel>)ExecuteStoredProcedureOrQueryGeneric(CommandType.Text, query, ArchivioFileModel.RecordToModelNoByte);
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(ElencoFileElaborati), ex);
                throw ex;
            }

            _Logger.DebugOut(nameof(ElencoFileElaborati), "elencoDaElaborare.Count: " + elencoElaborati.Count);

            return elencoElaborati;
        }

        public ArchivioFileModel GetById(ArchivioFileModel source)
        {
            _Logger.DebugIn(nameof(GetById), "source:" + source.ToString());

            ArchivioFileModel archivio = new ArchivioFileModel();
            string query = @"SELECT * FROM CB_ARCHIVIO_FILE WHERE id_archivio = :id_archivio";

            try
            {
                List<ArchivioFileModel> elencoDaElaborare = new List<ArchivioFileModel>();
                var parameters = new[]{
                    new OracleParameter(){ ParameterName = "id_archivio", Value = source.IdArchivio },
                };

                elencoDaElaborare = (List<ArchivioFileModel>)ExecuteStoredProcedureOrQueryGeneric(CommandType.Text, query, ArchivioFileModel.RecordToModel, parameters);
                archivio = elencoDaElaborare.First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
            }

            _Logger.DebugOut(nameof(GetById), "OUT archivio: " + archivio.ToString());

            return archivio;
        }

        public List<ArchivioFileModel> Retrieve()
        {
            throw new NotImplementedException();
        }

        public int Update(ArchivioFileModel source)
        {
            _Logger.DebugIn(nameof(Update), "source" + source.ToString());

            int aggiornato = 0;

            string query = @"
                                UPDATE CIM_INTEGRATION.CB_ARCHIVIO_FILE
                                SET    
                                       NOME_FILE              = :NOME_FILE,
                                       MIME_FILE              = :MIME_FILE,
                                       BYTE                   = :BYTE,
                                       DATE_CARICAMENTO       = :DATE_CARICAMENTO,
                                       ID_FLUSSO              = :ID_FLUSSO,
                                       DATE_FLUSSO_DA         = :DATE_FLUSSO_DA,
                                       DATE_FLUSSO_A          = :DATE_FLUSSO_A,
                                       ID_UTENTE_CARICAMENTO  = :ID_UTENTE_CARICAMENTO,
                                       ELABORATO              = :ELABORATO,
                                       DATE_ELABORAZIONE      = :DATE_ELABORAZIONE,
                                       ID_UTENTE_ELABORAZIONE = :ID_UTENTE_ELABORAZIONE
                                WHERE  ID_ARCHIVIO            = :ID_ARCHIVIO";

            try
            {
                var parameters = new[]{
                    new OracleParameter(){ ParameterName = "nome_file", Value = source.NomeFile },
                    new OracleParameter(){ ParameterName = "mime_file", Value = source.MimeFile },
                    new OracleParameter(){ ParameterName = "byte", Value = source.ByteFile, OracleDbType = OracleDbType.Blob },
                    new OracleParameter(){ ParameterName = "date_caricamento", Value = source.DataCaricamento },
                    new OracleParameter(){ ParameterName = "id_flusso", Value = source.IdFlusso },
                    new OracleParameter(){ ParameterName = "date_flusso_da", Value = source.DataFlussoDa },
                    new OracleParameter(){ ParameterName = "date_flusso_a", Value = source.DataFlussoDa },
                    new OracleParameter(){ ParameterName = "id_utente_caricamento", Value = source.IdUtenteCaricamento },
                    new OracleParameter(){ ParameterName = "elaborato", Value = source.Elaborato },
                    new OracleParameter(){ ParameterName = "date_elaborazione", Value = source.DataElaborazione },
                    new OracleParameter(){ ParameterName = "id_utente_elaborazione", Value = source.IdUtenteElaborazione },
                    new OracleParameter(){ ParameterName = "id_archivio", Value = source.IdArchivio },
                };

                aggiornato = ExecuteNonStoredProcedureOrQuery(CommandType.Text, query, parameters);
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(Update), ex);
                throw ex;
            }

            _Logger.DebugOut(nameof(Update), "aggiornato: " + aggiornato);

            return aggiornato;
        }
    }
}
