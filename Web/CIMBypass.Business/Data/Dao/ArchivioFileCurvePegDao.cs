﻿using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace Enel.Indra.CIMBypass.Business.Data.Dao
{
    public class ArchivioFileCurvePegDao : OracleDbConnector, IArchivioFileCurvePegDao
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("ArchivioFileCurvePegDao");
        public int Create(ArchivioFileCurvePegModel source)
        {
            _Logger.DebugIn(nameof(Create), "source" + source.ToString());

            int inserito = 0;

            string query = @"
INSERT INTO CB_ARCHIVIO_FILE_CURVE_PEG
(
 id_archivio_file,
 SKID,
 SKNUM,
 TERID,
 SEZID,
 COMBA,
 CALOA0,
 CALOA1,
 CALOA2,
 COMBB,
 CALOB0,
 CALOB1,
 CALOB2,
 COMBC,
 CALOC0,
 CALOC1,
 CALOC2,
 AREID,
 COCMBA,
 COCMBB,
 COCMBC,
 CFCMBA,
 CFCMBB,
 CFCMBC
 )
 VALUES
 (
 :id_archivio_file,
 :SKID,
 :SKNUM,
 :TERID,
 :SEZID,
 :COMBA,
 :CALOA0,
 :CALOA1,
 :CALOA2,
 :COMBB,
 :CALOB0,
 :CALOB1,
 :CALOB2,
 :COMBC,
 :CALOC0,
 :CALOC1,
 :CALOC2,
 :AREID,
 :COCMBA,
 :COCMBB,
 :COCMBC,
 :CFCMBA,
 :CFCMBB,
 :CFCMBC 
 )";

            try
            {
                var parameters = new[]{
                    new OracleParameter(){ ParameterName = "id_archivio_file", Value = source.idArchivioFile },
                    new OracleParameter(){ ParameterName = "SKID", Value = source.Skid },
                    new OracleParameter(){ ParameterName = "SKNUM", Value = source.Sknum },
                    new OracleParameter(){ ParameterName = "TERID", Value = source.Terid},
                    new OracleParameter(){ ParameterName = "SEZID", Value = source.Sezid },
                    new OracleParameter(){ ParameterName = "COMBA", Value = source.Comba },
                    new OracleParameter(){ ParameterName = "CALOA0", Value = source.Caloa0 },
                    new OracleParameter(){ ParameterName = "CALOA1", Value = source.Caloa1 },
                    new OracleParameter(){ ParameterName = "CALOA2", Value = source.Caloa2 },
                    new OracleParameter(){ ParameterName = "COMBB", Value = source.Combb },
                    new OracleParameter(){ ParameterName = "CALOB0", Value = source.Calob0 },
                    new OracleParameter(){ ParameterName = "CALOB1", Value = source.Calob1 },
                    new OracleParameter(){ ParameterName = "CALOB2", Value = source.Calob2 },
                    new OracleParameter(){ ParameterName = "COMBC", Value = source.Combc },
                    new OracleParameter(){ ParameterName = "CALOC0", Value = source.Caloc0 },
                    new OracleParameter(){ ParameterName = "CALOC1", Value = source.Caloc1 },
                    new OracleParameter(){ ParameterName = "CALOC2", Value = source.Caloc2 },
                    new OracleParameter(){ ParameterName = "AREID", Value = source.Areid },
                    new OracleParameter(){ ParameterName = "COCMBA", Value = source.Cocmba },
                    new OracleParameter(){ ParameterName = "COCMBB", Value = source.Cocmbb },
                    new OracleParameter(){ ParameterName = "COCMBC", Value = source.Cocmbc },
                    new OracleParameter(){ ParameterName = "CFCMBA", Value = source.Cfcmba },
                    new OracleParameter(){ ParameterName = "CFCMBB", Value = source.Cfcmbb },
                    new OracleParameter(){ ParameterName = "CFCMBC", Value = source.Cfcmbc },
                };

                inserito = ExecuteNonStoredProcedureOrQuery(CommandType.Text, query, parameters);
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(Create), ex);
                throw ex;
            }

            _Logger.DebugOut(nameof(Create), "inserito: " + inserito);

            return inserito;
        }

        public int Delete(ArchivioFileCurvePegModel source)
        {
            throw new NotImplementedException();
        }

        public ArchivioFileCurvePegModel GetById(ArchivioFileCurvePegModel source)
        {
            throw new NotImplementedException();
        }

        public List<ArchivioFileCurvePegModel> Retrieve()
        {
            throw new NotImplementedException();
        }

        public int Update(ArchivioFileCurvePegModel source)
        {
            throw new NotImplementedException();
        }
    }
}
