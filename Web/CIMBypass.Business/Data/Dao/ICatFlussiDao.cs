﻿using Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using System.Collections.Generic;

namespace Enel.Indra.CIMBypass.Business.Data.Dao
{
    public interface ICatFlussiDao : ICrudDao<CatFlussiModel>
    {
        List<CatFlussiModel> GetElencoAttivi();
    }
}
