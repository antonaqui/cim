﻿using System;
using System.Collections.Generic;
using System.Data;
using Enel.Indra.CIMBypass.Business.Data.DataConnectorFramework;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using System.Linq;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Oracle.ManagedDataAccess.Client;

namespace Enel.Indra.CIMBypass.Business.Data.Dao
{
    public class CatFlussiDao : OracleDbConnector, ICatFlussiDao
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("CatFlussiDao");

        public CatFlussiDao()
        {
        }

        public int Create(CatFlussiModel source)
        {
            throw new NotImplementedException();
        }

        public int Delete(CatFlussiModel source)
        {
            throw new NotImplementedException();
        }

        public CatFlussiModel GetById(CatFlussiModel source)
        {
            _Logger.DebugIn(nameof(GetById), "source:" + source.ToString());

            CatFlussiModel flusso = new CatFlussiModel();
            string query = @"SELECT * FROM CB_CAT_FLUSSI WHERE id_flusso = :id_flusso";

            try
            {
                List<CatFlussiModel> elencoFlussi = new List<CatFlussiModel>();
                var parameters = new[]{
                    new OracleParameter(){ ParameterName = "id_flusso", Value = source.IdFlusso },
                };

                elencoFlussi = (List<CatFlussiModel>)ExecuteStoredProcedureOrQueryGeneric(CommandType.Text, query, CatFlussiModel.RecordToModel, parameters);
                flusso = elencoFlussi.First();
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(GetById), ex);
            }

            _Logger.DebugOut(nameof(GetById), "OUT archivio: " + flusso.ToString());

            return flusso;
        }

        public List<CatFlussiModel> GetElencoAttivi()
        {
            _Logger.DebugIn(nameof(GetElencoAttivi), "NO PARAMETER");

            List<CatFlussiModel> elencoAttivi = new List<CatFlussiModel>();
            string query = @"SELECT * FROM CB_CAT_FLUSSI WHERE ATTIVO='Y'";

            try
            {
                elencoAttivi = (List<CatFlussiModel>)ExecuteStoredProcedureOrQueryGeneric(CommandType.Text, query, CatFlussiModel.RecordToModel);
            }
            catch (Exception ex)
            {
                _Logger.Error(nameof(GetElencoAttivi), ex);
            }

            _Logger.DebugOut(nameof(GetElencoAttivi), "elencoAttivi.Count: " + elencoAttivi.Count);

            return elencoAttivi;
        }

        public List<CatFlussiModel> Retrieve()
        {
            throw new NotImplementedException();
        }

        public int Update(CatFlussiModel source)
        {
            throw new NotImplementedException();
        }
    }
}
