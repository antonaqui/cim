﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enel.Indra.CIMBypass.Business.Data.Configuration
{
    public class ConnectionString
    {
        private string _ConnectionString;

        public string connectionString
        {
            get { return _ConnectionString; }
            set { _ConnectionString = value; }
        }
    }
}
