﻿namespace Enel.Indra.CIMBypass.Business.RuntimeExcecution
{
    interface IExecutableRuntime
    {
        string valida();
        string salvaSuDB(byte[] file, int idArchivio);
    }
}
