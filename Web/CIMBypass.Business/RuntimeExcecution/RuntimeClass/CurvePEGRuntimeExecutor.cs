﻿using Enel.Indra.CIMBypass.Business.Common.Utility;
using System.IO;
using Enel.Indra.CIMBypass.Business.Data.Model.CustomModel;
using Enel.Indra.CIMBypass.Business.Data.Dao;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;

namespace Enel.Indra.CIMBypass.Business.RuntimeExcecution.RuntimeClass
{
    public class CurvePEGRuntimeExecutor : IExecutableRuntime
    {
        private readonly IArchivioFileCurvePegDao _ArchivioFileCurvePegDaodao = new ArchivioFileCurvePegDao();
        private int idArchivio { get; set; }

        public string salvaSuDB(byte[] contenutoFile, int idArchivio)
        {
            this.idArchivio = idArchivio;

            string testoFile = FileUtility.DaByteATesto(contenutoFile);

            string linea;
            StringReader strReader = new StringReader(testoFile);
            while (true)
            {
                linea = strReader.ReadLine();
                if (linea != null)
                {
                    string casistista = GetSknum(linea);

                    if (casistista.Equals("54"))
                    {
                        InserisciTer54(linea);
                    }
                    else {
                        InserisciTer56(linea);
                    }
                }
                else
                {
                    break;
                }
            }

            return "ok";
        }

        private bool InserisciTer54(string linea)
        {
            CurvePeg54Model model = new CurvePeg54Model(linea);
            ArchivioFileCurvePegModel modelloConvertito = model.ConvertiInArchivioFileCurvePegModel();
            modelloConvertito.idArchivioFile = this.idArchivio;
            _ArchivioFileCurvePegDaodao.Create(modelloConvertito);
            return true;
        }
        private bool InserisciTer56(string linea)
        {
            CurvePeg56Model model = new CurvePeg56Model(linea);
            ArchivioFileCurvePegModel modelloConvertito = model.ConvertiInArchivioFileCurvePegModel();
            modelloConvertito.idArchivioFile = this.idArchivio;
            _ArchivioFileCurvePegDaodao.Create(modelloConvertito);
            return true;
        }

        private string GetSknum(string linea) { var sknum = linea.Substring(4, 2); return sknum; }

        public string valida()
        {
            throw new System.NotImplementedException();
        }
    }
}
