﻿using System;
using System.Reflection;

namespace Enel.Indra.CIMBypass.Business.RuntimeExcecution
{
    public class ExecutableRuntime
    {
        public ExecutableRuntime(string methodToInvoke)
        {
            this.methodToInvoke = methodToInvoke;
        }

        public ExecutableRuntime() { }

        public string methodToInvoke { get; set; }

        public string invokeMethod(String nameSpace, String className, object[] oggetti = null)
        {
            string fullyQualifiedName = nameSpace + "." + className;
            Type classType = Type.GetType(fullyQualifiedName);
            return doInvoke(classType, oggetti);
        }

        public string invokeMethod(String classWithNameSpace, object[] oggetti = null)
        {
            Type classType = Type.GetType(classWithNameSpace);
            return doInvoke(classType, oggetti);
        }

        private string doInvoke(Type classType, object[] oggetti = null)
        {
            Object istance = Activator.CreateInstance(classType);
            MethodInfo methodInfo = classType.GetMethod(methodToInvoke);
            if (oggetti != null)
            {
                var returnValue = methodInfo.Invoke(istance, oggetti);
                return returnValue.ToString();
            }
            else
            {
                var returnValue = methodInfo.Invoke(istance, null);
                return returnValue.ToString();
            }
        }
    }
}
