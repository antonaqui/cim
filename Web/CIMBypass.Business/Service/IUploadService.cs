﻿using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using System.Collections.Generic;

namespace Enel.Indra.CIMBypass.Business.Service
{
    public interface IUploadService : IBaseService
    {
        bool UploadFile(ArchivioFileModel file);
    }
}
