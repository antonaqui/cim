﻿using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using System.Collections.Generic;

namespace Enel.Indra.CIMBypass.Business.Service
{
    public interface IActionService : IBaseService
    {
        List<ArchivioFileModel> PrelevaElencoFileDaElaborare();
        List<ArchivioFileModel> PrelevaElencoFileElaborati();
        bool SalvaInTabella(int idArchivio);
    }
}
