﻿using System;
using System.Collections.Generic;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Data;
using Enel.Indra.CIMBypass.Business.Data.Dao;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;

namespace Enel.Indra.CIMBypass.Business.Service
{
    public class UploadService : BaseService, IUploadService
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("UploadService");
        public override Log4NetLogger Logger { get { return _Logger; } }

        private readonly ICatFlussiDao _CatFlussiDao;
        private readonly IArchivioFileDao _ArchivioFileDao;

        public UploadService(ICatFlussiDao CatFlussiDao, IArchivioFileDao ArchivioFileDao) : base(CatFlussiDao)
        {
            _CatFlussiDao = CatFlussiDao;
            _ArchivioFileDao = ArchivioFileDao;
        }

        public bool UploadFile(ArchivioFileModel file)
        {
            _Logger.DebugIn(nameof(UploadFile), "file:" + file.ToString());

            bool done = true;

            try
            {
                _ArchivioFileDao.Create(file);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex.Message, ex);
                done = false;
            }

            _Logger.DebugOut(nameof(UploadFile), "done:" + done);

            return done;
        }
    }
}
