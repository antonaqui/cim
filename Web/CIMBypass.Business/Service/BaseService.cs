﻿using System;
using System.Collections.Generic;
using Enel.Indra.CIMBypass.Business.Common.Log;

using Enel.Indra.CIMBypass.Business.Common.Utility;
using Enel.Indra.CIMBypass.Business.Data;
using Enel.Indra.CIMBypass.Business.Data.Dao;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;

namespace Enel.Indra.CIMBypass.Business.Service
{
    /// <summary>
    /// servizio custom, contiene log e currentThread
    /// </summary>
    public abstract class BaseService : IBaseService
    {
        public abstract Log4NetLogger Logger { get; }
        private readonly Log4NetLogger _Logger = new Log4NetLogger("BaseService");
        public readonly DateTime DaylightOn = DateUtility.GetWhenDaylightSwitchOn();
        public readonly DateTime DaylightOff = DateUtility.GetWhenDaylightSwitchOff();
        private readonly ICatFlussiDao _CatFlussiDao;

        public BaseService(ICatFlussiDao CatFlussiDao)
        {
            _CatFlussiDao = CatFlussiDao;
        }

        public List<CatFlussiModel> PrelevaElencoFlussiAttivi()
        {
            _Logger.DebugIn(nameof(PrelevaElencoFlussiAttivi), "NO PARAMETER");

            List<CatFlussiModel> ElencoFlussi = new List<CatFlussiModel>();

            try
            {
                ElencoFlussi = _CatFlussiDao.GetElencoAttivi();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex.Message, ex);
            }

            _Logger.DebugOut(nameof(PrelevaElencoFlussiAttivi), "ElencoFlussi.count: " + ElencoFlussi.Count);

            return ElencoFlussi;
        }
    }
}