﻿using System.Collections.Generic;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Enel.Indra.CIMBypass.Business.Data.Model.CustomModel;

namespace Enel.Indra.CIMBypass.Business.Service
{
    public interface IBaseService
    {
        Log4NetLogger Logger { get; }
        List<CatFlussiModel> PrelevaElencoFlussiAttivi();
        //List<CatMercatiModel> PrelevaElencoMercatiAttivi();
    }
}
