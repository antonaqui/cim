﻿using System.Collections.Generic;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Data;
using Enel.Indra.CIMBypass.Business.Data.Dao;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Enel.Indra.CIMBypass.Business.RuntimeExcecution;

namespace Enel.Indra.CIMBypass.Business.Service
{
    public class ActionService : BaseService, IActionService
    {
        private readonly Log4NetLogger _Logger = new Log4NetLogger("UploadService");
        public override Log4NetLogger Logger { get { return _Logger; } }

        private readonly IArchivioFileDao _ArchivioFileDao;
        private readonly ICatFlussiDao _CatFlussiDao;

        public ActionService(IArchivioFileDao ArchivioFileDao, ICatFlussiDao CatFlussiDao) : base(CatFlussiDao)
        {
            _ArchivioFileDao = ArchivioFileDao;
            _CatFlussiDao = CatFlussiDao;
        }

        public List<ArchivioFileModel> PrelevaElencoFileDaElaborare()
        {
            _Logger.DebugIn(nameof(PrelevaElencoFileDaElaborare), "NO PARAMETER");

            List<ArchivioFileModel> elencoFileDaElaborare;
            elencoFileDaElaborare = _ArchivioFileDao.ElencoFileDaElaborare();

            _Logger.DebugOut(nameof(PrelevaElencoFileDaElaborare), "elencoFileDaElaborare.Count: " + elencoFileDaElaborare.Count);

            return elencoFileDaElaborare;
        }

        public bool SalvaInTabella(int idArchivio)
        {
            _Logger.DebugIn(nameof(SalvaInTabella), "idCaricamento: " + idArchivio);

            ArchivioFileModel archivio = _ArchivioFileDao.GetById(new ArchivioFileModel() { IdArchivio = idArchivio });
            CatFlussiModel flusso = _CatFlussiDao.GetById(new CatFlussiModel() { IdFlusso = archivio.IdFlusso });

            ExecutableRuntime invocator = new ExecutableRuntime("salvaSuDB");
            string returnValue = invocator.invokeMethod(flusso.ClasseRuntime, new object[] { archivio.ByteFile, idArchivio });
            archivio.Elaborato = "S";

            _ArchivioFileDao.Update(archivio);
            _Logger.DebugOut(nameof(SalvaInTabella), "Salvataggio in: " + flusso.TargetTable);

            return true;
        }

        public List<ArchivioFileModel> PrelevaElencoFileElaborati()
        {
            _Logger.DebugIn(nameof(PrelevaElencoFileElaborati), "NO PARAMETER");

            List<ArchivioFileModel> elencoFileElaborati;
            elencoFileElaborati = _ArchivioFileDao.ElencoFileElaborati();

            _Logger.DebugOut(nameof(PrelevaElencoFileElaborati), "elencoFileElaborati.Count: " + elencoFileElaborati.Count);

            return elencoFileElaborati;
        }
    }
}
