﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Microsoft.AspNetCore.Mvc;

namespace Enel.Indra.CIMBypass.Controllers
{
    public class HomeController : BaseController
    {
        private static readonly Log4NetLogger _Logger = new Log4NetLogger("BaseController");

        public IActionResult Index()
        {
            _Logger.DebugIn(nameof(Index), "NO PARAMETER");
            return View();
        }

        public IActionResult ErrorPage()
        {
            _Logger.DebugIn(nameof(ErrorPage), "NO PARAMETER");
            return View();
        }
    }
}