﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Common.Utility;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Enel.Indra.CIMBypass.Business.Service;
using Enel.Indra.CIMBypass.Models;
using Enel.Indra.CIMBypass.Models.Common;
using Enel.Indra.CIMBypass.Models.Upload;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Enel.Indra.CIMBypass.Controllers
{
    public class UploadController : BaseController
    {
        private IUploadService _Service;
        private static readonly Log4NetLogger _Logger = new Log4NetLogger("UploadController");

        public UploadController(IUploadService IUploadService)
        {
            this._Service = IUploadService;
        }

        [HttpGet]
        public IActionResult UploadFile()
        {
            _Logger.DebugIn(nameof(UploadFile), "NO PARAMETER");

            UploadVModel campiPagina = new UploadVModel();
            List<CatFlussiModel> elencoFlussi = _Service.PrelevaElencoFlussiAttivi();
            campiPagina.InizializzaElencoFlussi(elencoFlussi);

            _Logger.DebugOut(nameof(UploadFile), "NO PARAMETER");

            return View(campiPagina);
        }

        [HttpPost]
        public IActionResult UploadFileBypass(IFormFile file)
        {
            _Logger.DebugIn(nameof(UploadFileBypass), "file input: " + file.Length);

            JsonUploadFileResponse risposta = new JsonUploadFileResponse();

            if (file == null || file.Length == 0)
                return Content("file not selected");

            string dataDa = HttpContext.Request.Form["indatetimepicker1"];
            string dataA = HttpContext.Request.Form["indatetimepicker2"];
            string idFlusso = HttpContext.Request.Form["indatetimepicker2"];
            byte[] contenutoFile;

            try
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    contenutoFile = ms.ToArray();
                }

                ArchivioFileModel archivio = new ArchivioFileModel();
                archivio.NomeFile = FileUtility.EstraiNomeFile(file.FileName);
                archivio.MimeFile = file.ContentType;
                archivio.ByteFile = contenutoFile;
                archivio.DataFlussoDa = DateUtility.FromStringToDateDMY(dataDa);
                archivio.DataFlussoA = DateUtility.FromStringToDateDMY(dataA);
                archivio.IdUtenteCaricamento = 1;
                archivio.IdFlusso = 1;

                _Service.UploadFile(archivio);

                risposta.isUploaded = SI;
                risposta.message = base.FILE_SUCCESS;
            }
            catch (Exception e)
            {
                risposta.isUploaded = NO;
                risposta.message = e.Message;
            }

            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(risposta));
        }
    }
}