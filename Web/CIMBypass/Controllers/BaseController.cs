using System;
using System.Collections.Generic;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Business.Common.Utility;
using Enel.Indra.CIMBypass.Business.Data.Model.CustomModel;
using Enel.Indra.CIMBypass.Business.Data.Model.Enumerator;
using Enel.Indra.CIMBypass.Business.Service;
using Enel.Indra.CIMBypass.Models.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;


namespace Enel.Indra.CIMBypass.Controllers
{
    /// <summary>
    /// Classe base generica per i controller
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseController: Controller
    {
        private readonly Log4NetLogger _Logger;

        public readonly string FILTER_KEY = "FILTER_KEY";
        public readonly string SI = EnumSiNo.Si.GetDescription();
        public readonly string NO = EnumSiNo.No.GetDescription();
        public readonly string FILE_SUCCESS = EnumOperationMessage.FILE_SUCCESS;
        public readonly string FILE_SUCCESS_SCARTI = EnumOperationMessage.FILE_SUCCESS_SCARTI;
        public readonly string FILE_ERROR = EnumOperationMessage.FILE_ERROR;
        public readonly string FILE_ERROR_TEMPLATE = EnumOperationMessage.FILE_ERROR_TEMPLATE;
        public readonly string OP_SUCCESS = EnumOperationMessage.OP_SUCCESS;
        public readonly string OP_ERROR = EnumOperationMessage.OP_ERROR;
        public readonly string OP_WARNING = EnumOperationMessage.OP_WARNING;
        public readonly string OP_OUTOFRANGE = EnumOperationMessage.OP_OUTOFRANGE;
        public readonly string ICON_SUCCESS = EnumIcon.ICON_SUCCESS;
        public readonly string ICON_ERROR = EnumIcon.ICON_ERROR;
        public readonly string ICON_WARNING = EnumIcon.ICON_WARNING;
        public readonly string ICON_OUTOFRANGE = EnumIcon.ICON_OUTOFRANGE;
        public readonly string ICON_INFORMATION = EnumIcon.ICON_INFORMATION;

        public string Matricola { get; set; }

        protected OperationResultModel OpResult
        {
            get { return TempData["OperationResult"] == null ? null : TempData["OperationResult"] as OperationResultModel; }
            set { TempData["OperationResult"] = value; }
        }

        protected BaseController()
        {
            _Logger = new Log4NetLogger(this.GetType().Name);

            //if (HttpContext.Current != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            //{
              //  MembershipUser usr = Membership.GetUser();
                //if (usr != null)
                //{
                    //this.Matricola = usr.UserName;
                //}
            //}
            //else
            //{
                Matricola = "666";
            //}
            //ewBag.Envirorment = ConfigurationManager.AppSettings["envirorment"];
        }

        protected void ManageResult(bool success)
        {
            
            string inputParameters = "[success: " + success + "]";
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, inputParameters);

            OperationResultModel result = new OperationResultModel()
            {
                Type = success ? EnumMessageType.Success : EnumMessageType.Warning,
                Message = success ? OP_SUCCESS : OP_WARNING,
                IconName = success ? ICON_SUCCESS : ICON_WARNING,
            };

            OpResult = result;

            
            string outputParameters = "No output parameters";
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, outputParameters);
        }

        protected void ManageMessage(string message)
        {
            
            string inputParameters = "[message: " + message + "]";
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, inputParameters);

            OperationResultModel result = new OperationResultModel()
            {
                Type = EnumMessageType.Information,
                IconName = ICON_INFORMATION
            };

            OpResult = result;

            
            string outputParameters = "No output parameters";
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, outputParameters);
        }

        protected void ManageMessageError(string message)
        {
            
            string inputParameters = "[message: " + message + "]";
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, inputParameters);

            OperationResultModel result = new OperationResultModel()
            {
                Message = message,
                Type = EnumMessageType.Error,
                IconName = ICON_ERROR
            };

            OpResult = result;

            
            string outputParameters = "No output parameters";
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, outputParameters);
        }

        protected void ManageOutOfRange(bool inRange)
        {
            
            string inputParameters = "[inRange: " + inRange + "]";
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, inputParameters);

            OperationResultModel result = new OperationResultModel()
            {
                Type = inRange ? EnumMessageType.None : EnumMessageType.Warning,
                Message = inRange ? OP_SUCCESS : OP_OUTOFRANGE,
                IconName = inRange ? ICON_SUCCESS : ICON_OUTOFRANGE,
            };

            OpResult = result;

            
            string outputParameters = "No output parameters";
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, outputParameters);
        }

        protected void ManageResult(Exception ex)
        {
            
            string inputParameters = "[ex: " + ex.Message + "]";
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, inputParameters);

            OperationResultModel result = new OperationResultModel()
            {
                Type = EnumMessageType.Error,
                Message = OP_ERROR,
                IconName = ICON_ERROR
            };

            OpResult = result;

            
            string outputParameters = "No output parameters";
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, outputParameters);
        }

        [HttpPost]
        public ActionResult Logger(JsonLogger value)
        {
            
            string inputParameters = "[value: " + value.ToString() + "]";
            _Logger.DebugIn(System.Reflection.MethodBase.GetCurrentMethod().Name, inputParameters);

            
            string outputParameters = "Return Json(success)";
            _Logger.DebugOut(System.Reflection.MethodBase.GetCurrentMethod().Name, outputParameters);

            return Json("Success");
        }

        public ActionResult LogExceptionAndRedirectToErrorPage(string controllerMethodName, Exception ex)
        {
            _Logger.Fatal(controllerMethodName, ex);
            ManageResult(ex);

            return RedirectToAction("ErrorPage", "Home");
        }

        public Dictionary<string, string> estraiForm(FormCollection form)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            foreach (var tmp in form.Keys)
            {
                dictionary.Add(tmp, Convert.ToString(form[tmp]));
            }

            return dictionary;
        }

        #region Searchable Service

        public Tfilter GetPreviousFilter<Tfilter>() where Tfilter : class
        {
            Tfilter toReturn = null;

            if (TempData[FILTER_KEY] != null && TempData[FILTER_KEY] is Tfilter)
            {
                toReturn = TempData[FILTER_KEY] as Tfilter;
                TempData[FILTER_KEY] = toReturn;
            }

            return toReturn;
        }

        public void SetPreviousFilter<Tfilter>(Tfilter filter) where Tfilter : class
        {
            TempData[FILTER_KEY] = filter;
        }
        #endregion

    }
}
