﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enel.Indra.CIMBypass.Business.Service;
using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Microsoft.AspNetCore.Mvc;
using Enel.Indra.CIMBypass.Models.Action;
using Newtonsoft.Json;
using Enel.Indra.CIMBypass.Business.Common.Log;
using Enel.Indra.CIMBypass.Models.Common;

namespace Enel.Indra.CIMBypass.Controllers
{
    public class ActionController : BaseController
    {

        private readonly Log4NetLogger _Logger = new Log4NetLogger("ActionController");
        private IActionService _Service;

        public ActionController(IActionService IActionService) {
            this._Service = IActionService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            _Logger.DebugOut(nameof(Index), "NO PARAMETER");

            IndexVModel modello = new IndexVModel();
            List<CatFlussiModel> elencoFlussiAttvi = _Service.PrelevaElencoFlussiAttivi();
            modello.InizializzaElencoFlussi(elencoFlussiAttvi);

            return View(modello);
        }
        
        [HttpPost]
        public IActionResult PrelevaElencoFileDaElaborare() {
            _Logger.DebugIn(nameof(PrelevaElencoFileDaElaborare), "NO PARAMETER");

            List<ArchivioFileModel> fileDaElaborare = _Service.PrelevaElencoFileDaElaborare();

            _Logger.DebugOut(nameof(PrelevaElencoFileDaElaborare), "fileDaElaborare: " + Json(fileDaElaborare).ToString());

            return Json(fileDaElaborare);
        }

        [HttpPost]
        public IActionResult PrelevaElencoFileElaborati()
        {
            _Logger.DebugIn(nameof(PrelevaElencoFileElaborati), "NO PARAMETER");

            List<ArchivioFileModel> fileElaborati = _Service.PrelevaElencoFileElaborati();

            _Logger.DebugOut(nameof(PrelevaElencoFileElaborati), "fileElaborati: " + Json(fileElaborati).ToString());

            return Json(fileElaborati);
        }

        [HttpGet]
        public IActionResult ElaboraFileInput(string idArchivio)
        {
            _Logger.DebugOut(nameof(ElaboraFileInput), "idArchivio: " + idArchivio);

            _Service.SalvaInTabella(Int32.Parse(idArchivio));

            return Json(true);
        }
    }
}