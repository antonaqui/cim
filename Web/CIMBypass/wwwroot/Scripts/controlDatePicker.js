﻿// Controlla che le date siano popolate
function isDatesNull(dataDa, dataA) {
    if (dataDa == "" || dataA == "") {
        return true;
    }
    else {
        return false;
    }
}

// Controlla che le date siano popolate in Gestione Asta
function isDatesNull(validoDal, validoAl) {
    if (validoDal == "" || validoAl == "") {
        return true;
    }
    else {
        return false;
    }
}

// Controlla che la start date sia minore uguale di end date
function isDateRangeNotValid(dataDa, dataA) {
    var partsDataDa = dataDa.split("/");
    var dataDaISO = partsDataDa[2] + "-" + partsDataDa[1] + "-" + partsDataDa[0];
    var dataDaObj = new Date(dataDaISO + "T00:00:00Z");

    var partsDataA = dataA.split("/");
    var dataAISO = partsDataA[2] + "-" + partsDataA[1] + "-" + partsDataA[0];
    var dataAObj = new Date(dataAISO + "T00:00:00Z");

    if (dataDaObj.getTime() > dataAObj.getTime()) {
        return true;
    }
    else {
        return false;
    }
}