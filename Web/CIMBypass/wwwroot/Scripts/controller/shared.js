﻿function manageClickGet() {
    $('[data-ajax-get]').off('click').on('click', function (el) {
        var action = $(el.currentTarget).data('ajax-get');
        $.ajax({
            type: 'GET',
            url: action,
            dataType: 'html',
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                $('.content').html(result);
            }
        });
    });

    $('[data-url-get]').off('click').on('click', function (el) {
        var action = $(el.currentTarget).data('url-get');
        location.href = action;
    });

}

function exitfromApp() {
    $('#myModal').modal('toggle');

    $('#myModal #save').off().on("click", function () { window.close(); })
}

function manageDatePicker() {
    $("[data-filter='datepicker']").datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'it',
        endDate: '0'
    });
}

var stop = false;

$(document).ready(function () {
    manageClickGet();
    manageDatePicker();
    $(".input-group-addon").on("click", function () { $(this).parent().find("input").focus() });

});

$(document).ajaxStop(function () {
    stop = true;
    spinner.hide();
    manageClickGet();
    manageDatePicker();
});

$(document).ajaxSend(function (event, jqXHR, ajaxOptions) {
    stop = false;
    if (ajaxOptions.context) {
        logger.logRequest(ajaxOptions.context);
    }
    else {
        logger.logRequest(ajaxOptions);
    }

    setTimeout(function () {
        if (!stop) {
            spinner.show();
        }
    }, 100);
});

$(document).ajaxError(function (event, jqXhr, ajaxOptions, thrownError) {
    stop = true;
    spinner.hide();

    var error =
        {
            responseText: jqXhr.responseText,
            thrownError: thrownError
        };

    logger.logError(error);
    //alert("Errore imprevisto: " + thrownError);
});

window.onerror = function (msg, source, lineno, colno, error) {
    spinner.hide();
    alert("Errore imprevisto: " + msg);
    logger.logError(msg + ' ' + source + ' ' + lineno + ' ' + colno + ' ' + error);
};


