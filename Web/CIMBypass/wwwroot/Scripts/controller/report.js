﻿var report = {
    id: $('input[data-reportid]').val(),
    urlSearchNew: $('[data-reportbutton]').data('reportactionsearchnew'),
    urlSearch: $('[data-reportbutton]').data('reportactionsearch'),
    urlExportExcel: $('[data-excelexportbutton]').data('reportactionexport'),
    urlDownloadExcel: "/Export/DownloadExcel",
    urlLoadFilter: "/Report/LoadFilter",
    validateFilter: function () {
      
        return $('[data-reportfilters]').validateParsley();
    },
    serializeReportFilter: function (selector) {
     
        var filter = selector ? $("[data-filter]" + selector) : $("[data-filter]");
        var result = [];
        filter.each(function (i, item) {

            
            var json = { "Name": $(item).attr('name'), "Value": $(item).val(), "PrevValue": $(item).data('prevvalue'), "ValueType": $(item).data('valuetype'), "Hidden": $(item).is(':hidden'), "Text": $(item).find('option:selected').text() === ''? $(item).val() : $(item).find('option:selected').text() };
            if ($(item).data('parents')) {
                var names = $(item).data('parents')
                    .split(',')
                    .map(function (n) { return "[name='" + n + "']"; })
                    .join();

                var parents = report.serializeReportFilter(names);
                json.Parents = { "Filters": parents };
            }

            result.push(json);
        });

        return result;
    },
    buildTabs: function (json) {
        $("div#tabs ul").empty();
        $("div#tabs").tabs();
        $('[data-reporttabs]').show();
        for (i = 0; i < json.numeroTab; i++) {
            //$("div#tabs ul").append("<li><a href='#tab'>" + json.labeltab[i] + "</a></li>");
            //$("div#tabs ul").append("<li><a href='javascript:processTab(this)'> Domanda " + (i + 1) + "</a><li>");
            if(json.tabType == "questionario")
                $("div#tabs ul").append("<li><a id='tab_" + (i + 1) + "' tabType='" + json.tabType + "' idDomanda='" + (i + 1) + "' numeroRisposte = '" + json.domande[i].numeroRisposte + "' domanda='" + json.domande[i].domanda + "' risposte='"+JSON.stringify(json.domande[i].risposte)+"' onclick='report.processTab(this);'> Domanda " + (i + 1) + "</a><li>");
        }
        
        //$("div#tabs").append(
        //            "<div id='tab" + num_tabs + "'>#" + num_tabs + "</div>"
        //        );
        $("div#tabs").tabs("refresh");


        report.processTab(document.getElementById("tab_1"));


    },
    buildTableByJson: function (json, tabWidget) {

        if (json.Header === "DB_Timeout_Exception") {
            throw new Error('Database Timeout');
            return;
        }

        var columns = $(json.Columns).map(function (i, item) {
            return { "data": item.Data, "type": item.Type };
        });


        var table = $("<table class='table table-hover table-bordered table-striped'></table>");
        $('[data-reportdata]').show();
        $('[data-tablewrap]').empty().append(table);

        //if ($.fn.DataTable.isDataTable(table)) {
        //    table.DataTable().destroy();
        //}

        if (json.DynamicHeader)
            json.Header = report.processDynamicHeader(json.Header, json.DynamicHeader, tabWidget);

        if (json.Rows[0] && json.Rows[0].length < $(json.Header).find("tr:last th").length) {
            json.Header = report.processVariableHeader(json.Header);
        }
        if (tabWidget && tabWidget.getAttribute("risposte")) {
            var legg = JSON.parse(tabWidget.getAttribute("risposte"));
            report.showLeggenda(legg);
        }
        if (json.ReportLegend) {
            //var legg = JSON.parse(json.ReportLegend);
            report.showLeggendaReport(json.ReportLegend);
        }

        table.append(json.Header);

        //Total row handler
        if (Object.keys(json).length) {
            for (var count = 0; count < json.Rows.length; count++) {
                if (json.Rows[count][0] === "TOTALE") {
                    var footerContent = "<tfoot id='tfoot_id'><tr style='text-align:center'>";
                    for (var i = 0; i < json.Rows[count].length; i++) {
                        footerContent += "<td>";
                        footerContent += json.Rows[count][i];
                        footerContent += "</td>";
                    }
                    footerContent += "</tr></tfoot>";
                    table.append(footerContent);
                    json.Rows.splice(count, 1);
                    break;
                }
            }
        }
        $("#tfoot_id").hide();
        ////

        table.DataTable({
            aoColumns: $.map($(json.Header).find("tr:last th"), function (el) { return { "sType": $(el).data("type") === undefined ? null : $(el).data("type") } }),
            processing: true,
            searching: false,
            language: {
                "sEmptyTable": "Nessun dato presente nella tabella",
                "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Visualizza _MENU_ elementi",
                "sLoadingRecords": "Caricamento...",
                "sProcessing": "Elaborazione...",
                "sSearch": "Cerca:",
                "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                "oPaginate": {
                    "sFirst": "Inizio",
                    "sPrevious": "Precedente",
                    "sNext": "Successivo",
                    "sLast": "Fine"
                },
                "oAria": {
                    "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                    "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                }
            },
            data: json.Rows,
            "fnDrawCallback": function () {
                var info = table.DataTable().page.info();
                if ((info.page + 1) == info.pages) {
                    $("#tfoot_id").show();
                } else {
                    $("#tfoot_id").hide();
                }
            }
        });
        table.wrap("<div class='scrolledTable'></div>");

        if (table.DataTable().page.info().pages == 1) {
            $("#tfoot_id").show();
        }

    },
    processVariableHeader: function (header) {
        var xmlHeader = report.getXmlDomParser(header);

        for (i = 0; i < xmlHeader.documentElement.childNodes.length; i++) { //per tutti i tr su cui operare
            //cerco i th con attributo variable
            for (y = 0; y < xmlHeader.documentElement.childNodes[i].childNodes.length; y++) {
                if (xmlHeader.documentElement.childNodes[i].childNodes[y].getAttribute('type') == 'variable') {
                    var childToRemove = xmlHeader.documentElement.childNodes[i].childNodes[y];
                    xmlHeader.documentElement.childNodes[i].removeChild(childToRemove);
                }
            }
        }

        return new XMLSerializer().serializeToString(xmlHeader.documentElement);

    },
    showLeggenda: function (json) {
        $("div#boxleggenda table tbody").empty();
        //$('[data-reportleggenda]').show();
        $('#idLegenda').css("display", "inline");
        $('#idLegenda').empty();
        $("#idLegenda").append("<label>LEGENDA</label><br>");
        //if ($("#idLegenda").children().length == 2) {
        $('#contenitoreLegenda').css("display", "block");
        var rows = "";
        for (var i = 0; i < json.length; i++) {
            rows += "<b>Risposta " + (i + 1) + ":</b>    " + json[i].risposta + "</br>";
        }
        $("#idLegenda").append(rows);
        //}
        //for(var i = 0; i < json.length;i++){
        //    $("div#boxleggenda table tbody").append("<tr><th>Risposta " + (i+1) + "</th><th>" + json[i].risposta + "</th></tr>");
        //}
    },
    showLeggendaReport: function (rows) {
        $("div#boxleggenda table tbody").empty();
        $('#idLegenda').css("display", "inline");
        $('#idLegenda').empty();
        $("#idLegenda").append("<label>LEGENDA</label><br>");
        //if ($("#idLegenda").children().length == 2) {
        $('#contenitoreLegenda').css("display", "block");
        $("#idLegenda").append(rows);
        //}
    },
    processDynamicHeader: function (header, dynamicHeader, tabWidget) {

        var xmlHeader = report.getXmlDomParser(header);

        var xmlDynHeader = report.getXmlDomParser(dynamicHeader);

        var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

        //aggiungo tanti elementi dinamici quante sono le risposte
        for (n = 0; n < tabWidget.getAttribute("numeroRisposte") ; n++) {
            for (i = 0; i < xmlDynHeader.documentElement.childNodes.length; i++) { //per tutti i tr su cui operare (quelli dell'header e del dynamicHeader coincidono)
                var headerDynamicTag = xmlHeader.documentElement.childNodes[i].getElementsByTagName("dynamic")[0]; //prendo il tag dynamic dell'i-esimo tag tr dell'          


                //inserisco tutti i figli del tr del dynamicHeader prima del tag dynamic dell'header
                for (y = 0; y < xmlDynHeader.documentElement.childNodes[i].childNodes.length; y++) {
                    var newNode = xmlDynHeader.documentElement.childNodes[i].childNodes[y].cloneNode(true);
                    if (isIE11) {
                        var innerText = $(newNode).text();
                        innerText = innerText.replace('$', "" + (n + 1));
                        $(newNode).text(innerText);
                    } else {
                        if (newNode.innerHTML)
                            newNode.innerHTML = newNode.innerHTML.replace('$', "" + (n + 1));
                    }
                    xmlHeader.documentElement.childNodes[i].insertBefore(newNode, headerDynamicTag);

                }


                //inseriti tutti i nodi, posso eliminare il tag dynamic
                //xmlHeader.documentElement.childNodes[i].removeChild(headerDynamicTag);

            }
        }

        for (i = 0; i < xmlDynHeader.documentElement.childNodes.length; i++) { //per tutti i tr su cui operare (quelli dell'header e del dynamicHeader coincidono)
            var headerDynamicTag = xmlHeader.documentElement.childNodes[i].getElementsByTagName("dynamic")[0]; //prendo il tag dynamic dell'i-esimo tag tr dell'
            xmlHeader.documentElement.childNodes[i].removeChild(headerDynamicTag);
        }

        return new XMLSerializer().serializeToString(xmlHeader.documentElement);

    },
    getXmlDomParser: function (xmlString) {
        var xmlHeader;

        if (window.DOMParser) {

            var parser = new DOMParser();

            xmlHeader = parser.parseFromString(xmlString, "text/xml");

        }

        else // Internet Explorer

        {

            xmlHeader = new ActiveXObject("Microsoft.XMLDOM");

            xmlHeader.async = false;

            xmlHeader.loadXML(xmlString);

        }

        return xmlHeader;
    },
    buildTableByJsonAsync: function (json) {
        var columns = $(json.Columns).map(function (i, item) {
            return { "data": item.Data, "type": item.Type };
        });



        var table = $("<table class='table table-hover table-bordered table-striped'></table>");
        $('[data-reportdata]').show();
        $('[data-tablewrap]').empty().append(table);

        var script = this;


        table.append(json.Header);

        table.DataTable({

            processing: true,
            serverSide: true,
            searching: false,
            responsive: true,
            scrollX: true,
            aoColumns: $.map($(json.Header).find("tr:last th"), function (el) { return { "sType": $(el).data("type") === undefined ? null : $(el).data("type") } }),
            language: {
                "sEmptyTable": "Nessun dato presente nella tabella",
                "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Visualizza _MENU_ elementi",
                "sLoadingRecords": "Caricamento...",
                "sProcessing": "Elaborazione...",
                "sSearch": "Cerca:",
                "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                "oPaginate": {
                    "sFirst": "Inizio",
                    "sPrevious": "Precedente",
                    "sNext": "Successivo",
                    "sLast": "Fine"
                },
                "oAria": {
                    "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                    "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                }
            },

            ajax: {
                url: ReportAsyncSearch,
                	
                contentType: 'application/json',
                type: 'POST',
                 data: function (d) {
                     d.idReport =  report.id,
                     d.filter = { Filters: report.serializeReportFilter() }
                     return JSON.stringify(d);
            }
            }
        });


    },
    getData : function (){

        return {
           
        }
    },
    processTab: function (tabWidget) {

        $("h4#domanda").empty();
        $("h4#domanda").append(tabWidget.innerHTML + " : " + tabWidget.getAttribute("domanda"));
        $('[data-reportdomanda]').show();

        $(".nav-tabs .active").removeClass("active");
        tabWidget.parentElement.setAttribute("class", "active");

        if (tabWidget.getAttribute("tabType") == "questionario") {
            $("#idDomanda").val(tabWidget.getAttribute("idDomanda"));
            $("#numeroRisposte").val(tabWidget.getAttribute("numeroRisposte"));
        }

        var data = JSON.stringify({
            idReport: this.id,
            filter: { Filters: this.serializeReportFilter() }
        });

        $.ajax({
            traditional: true,
            type: 'POST',
            url: this.urlSearch,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (result) {
                report.buildTableByJson(result, tabWidget);
            },
        });

    },
    search: function () {

        var validation = this.validateFilter();
        if (!validation)
            return;

        var dataDa;
        var dataA;

        $('[data-filter]').each(function (i, item) {
            $(item).data('prevvalue', $(item).val());
            if ($(item).data('filter') == 'datepicker' && ($(item).attr('id').toLowerCase() == 'datada' || $(item).attr('id').toLowerCase() == 'data_da')) {
                dataDa = $(item);
            } else if ($(item).data('filter') == 'datepicker' && ($(item).attr('id').toLowerCase() == 'dataa' || $(item).attr('id').toLowerCase() == 'data_a')) {
                dataA = $(item);
            }
        });

        if (dataDa != undefined) {
            var dDATokens = dataDa.val().split('/');
            var dATokens = dataA.val().split('/');

            var dDA = new Date(Number(dDATokens[2]), Number(dDATokens[1])-1, Number(dDATokens[0]), 0, 0, 0);
            var dA = new Date(Number(dATokens[2]), Number(dATokens[1])-1, Number(dATokens[0]), 0, 0, 0);

            if (dDA > dA) {
                dataDa.removeClass('has-success');
                dataA.removeClass('has-success');
                dataDa.addClass('has-error');
                dataA.addClass('has-error');
                dataDa.css('border', '1px solid red');
                dataA.css('border', '1px solid red');

                dataDa.tooltip('destroy');
                dataDa.tooltip({
                    animation: false,
                    container: 'body',
                    placement: 'top',
                    title: 'DATA DA non può essere maggiore di DATA A'
                });
                dataDa.tooltip('show');

                dataA.tooltip('destroy');
                dataA.tooltip({
                    animation: false,
                    container: 'body',
                    placement: 'top',
                    title: 'DATA A non può essere minore di DATA DA'
                });
                dataA.tooltip('show');

                return;
            }
        }

        var data = JSON.stringify({
            idReport: this.id,
            filter: { Filters: this.serializeReportFilter() }
        });
        

        $.ajax({
            traditional: true,
            type: 'POST',
            url: this.urlSearchNew,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (result) {

                if (result.creaTab)
                    report.buildTabs(result);
                else
                    report.buildTableByJson(result);
            },
        });
    },
    exportExcel: function () {

        var data = JSON.stringify({
            idReport: this.id,
            filter: { Filters: this.serializeReportFilter() }
        });



        $.ajax({
            traditional: true,
            type: 'POST',
            url: this.urlExportExcel,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (result) {
           
                window.location = window.applicationBaseUrl+report.urlDownloadExcel + '?fileGuid=' + result.FileGuid
                              + '&filename=' + result.FileName;

            },
            error: function (jqXhr) {
           
                logger.logError(jqXhr);
            }

        });
    },

    saveExcelOnDb: function(){

        var data = JSON.stringify({
            idReport: this.id,
            filter: { Filters: this.serializeReportFilter() }
        });

        $.ajax({
            traditional: true,
            type: 'POST',
            url: this.urlExportExcel,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (result) {
                
                alert(result);

            },
            error: function (jqXhr) {
           
                logger.logError(jqXhr);
            }

        });
    },

    loadFilter: function (selector) {
        var flt = $(selector);
        var data = {
            idReport: this.id,
            filter: this.serializeReportFilter(selector)[0],
        };

        $.ajax({
            traditional: true,
            type: 'POST',
            url: this.urlLoadFilter,
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (result) {
                if (result.DataSource && flt.data('filter') == 'picklist') {
                    flt.html('');

                    $.each(result.DataSource, function (i, item) {
                        flt.append($('<option>', {
                            value: item.Key,
                            text: item.Value
                        }));
                    });

                    if (result.DataSource.length > 1) {
                        flt.removeAttr('data-toggle');
                    } else {
                        flt.attr('data-toggle', 'tooltip');
                    }
                    

                }
                else {
                    flt.val(result.Value);
                }
                spinner.hide();
                setTimeout(function () { flt.trigger('change'); }, 500);
                
                
            }
        });
    },
    reportize: function () {
        
        $('[data-filter][data-children]').off('change').on('change', function () {
            var names = $(this).data('children').split(',');
            for (var i = 0; i < names.length; i++) {
                report.loadFilter("[name='" + names[i] + "']");
                
            }
        });

        $('[data-reportbutton').off('click').on('click', function () {
            report.search();
        });

        $('[data-excelexportbutton').off('click').on('click', function () {
            report.exportExcel();
            //report.saveExcelOnDb();
        });
    }
}

$(document).ready(function () {
 
   
    if (ReportLoadFilter) {
        report.urlLoadFilter = ReportLoadFilter;
    }
    report.reportize();
    $.listen('parsley:field:error', function (fieldInstance) {
        var messages = ParsleyUI.getErrorsMessages(fieldInstance);
        fieldInstance.$element.tooltip('destroy');
        fieldInstance.$element.tooltip({
            animation: false,
            container: 'body',
            placement: 'top',
            title: messages
        });
        fieldInstance.$element.css("border", " 1px solid red");
    });

    // destroy tooltip when field is valid
    $.listen('parsley:field:success', function (fieldInstance) {
        fieldInstance.$element.tooltip('destroy');
        fieldInstance.$element.css("border", " 1px solid #ccc");
    });

    $("div#tabs").tabs();

    //jQuery.extend(jQuery.fn.dataTableExt.oSort, {
       
    //    "date-it-pre": function (a) {
            
    //        var itDatea = a.split('/');
    //        if (itDatea.length == 3)
    //            return (itDatea[2] + itDatea[1] + itDatea[0]) * 1;
    //        else if (itDatea.length == 2)
    //            return ('01' + itDatea[1] + itDatea[0]) * 1;
    //        else if (itDatea.length == 1 && itDatea[0] == "TOTALE")
    //            return ('31' + '01' + '2030') * 1;
    //        else if (itDatea.length == 1 && itDatea[0] != "TOTALE")
    //            return ('01' + '01' + itDatea[0]) * 1;
    //    },

    //    "date-it-asc": function (a, b) {
            
    //        return ((a < b) ? -1 : ((a > b) ? 1 : 0));

    //    },

    //    "date-it-desc": function (a, b) {
    //        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    //    }
    //});

});

