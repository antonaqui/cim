﻿    
var logger = {
    url: '/Base/Logger',
    logError: function (error) {
        var req = JSON.stringify(error);
        $.ajax({
            url: this.url,
            data: JSON.stringify({ 'value': req, 'isError': true }),
            global: false,
            dataType: 'json',
            traditional: true,
            contentType: "application/json; charset=utf-8",
            type: 'POST'
        });
    },
    logRequest: function (request) {
        var req = JSON.stringify(request);
        $.ajax({
            url: this.url,
            data: JSON.stringify({ 'value': req, 'isError': false }),
            global: false,
            traditional: true,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'POST'
        });
    }
};