﻿$(document).ready(function () {
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {

        "date-it-pre": function (a) {

            var itDatea = a.split('/');
            if (itDatea.length == 3)
                return (itDatea[2] + itDatea[1] + itDatea[0]) * 1;
            else if (itDatea.length == 2)
                return ('01' + itDatea[1] + itDatea[0]) * 1;
            else if (itDatea.length == 1 && itDatea[0] == "TOTALE")
                return ('31' + '01' + '2030') * 1;
            else if (itDatea.length == 1 && itDatea[0] != "TOTALE")
                return ('01' + '01' + itDatea[0]) * 1;
        },

        "date-it-asc": function (a, b) {

            return ((a < b) ? -1 : ((a > b) ? 1 : 0));

        },

        "date-it-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },

        "reg-it-pre": function (a) {

          if (a == "TOTALE")
              return ('Z');
          return a;
        },

        "reg-it-asc": function (a, b) {
            if (a == "TOTALE")
                return -1;
            if (b == "TOTALE")
                return 1;
            var c = a.attr.localeCompare(b.attr);
            if (c == a)
                return b;
            else
                return a;

        },

        "reg-it-desc": function (a, b) {
            if (a == "TOTALE")
                return -1;
            if (b == "TOTALE")
                return 1;

            return a.attr.localeCompare(b.attr);
        }

    });
});