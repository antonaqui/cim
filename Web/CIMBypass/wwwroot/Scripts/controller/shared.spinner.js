﻿var spinner = {
    //var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    //pleaseWaitDiv: $('<div class="modal fade bs-modal-sm" id="myPleaseWait" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><span class="glyphicon glyphicon-time"></span>&nbsp; Attendere Prego...</h4></div><div class="modal-body"><div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 100%"></div></div></div></div></div></div>'),
    pleaseWaitDiv: $('<div class="modal fade bs-modal-sm" id="myPleaseWait" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"><div class="modal-dialog modal-sm"><div class="box box-primary spinner-box"><div class="box-body"><div class="modal-header spinner-border"><h5 class="modal-title spinner-title">&nbsp; Attendere Prego...<i class="fa fa-spinner fa-spin pull-right" style="font-size:45px; color:#ff0f64;"></i></h4></div></div></div></div></div></div>'),
    countSpinner: 0,
    show: function () {
        console.log("entra count spinner show", this.countSpinner);
        if (!this.isVisible()) {
            this.pleaseWaitDiv.modal('show');
        }
    },

    hide: function () {
        this.pleaseWaitDiv.modal('hide');
        $('.modal-backdrop').remove();
    },
    isVisible: function () {
        return this.pleaseWaitDiv.hasClass('in');
    }
};
