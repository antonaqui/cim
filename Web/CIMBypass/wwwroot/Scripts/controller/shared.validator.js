﻿(function ($) {
    $.fn.validateParsley = function (options) {
        var form = $(this).parents('form[data-val-validate]').eq(0);
        if (form.length > 0) {
            var config = {
                excluded: 'input[type=button], input[type=submit], input[type=reset], input[type=hidden], [disabled], [data-val-disabled], [data-val-disabled] *, :hidden',
                included: 'textarea, input[type=text], select, input[type=check]',
                successClass: 'has-success',
                errorClass: 'has-error',
              
                errors: {
                    classHandler: function (el) {
                        return el.parent();
                    },
                    // Set these to empty to make sure the default Parsley elements are not rendered
                    errorsWrapper: '',
                    errorElem: ''
                },
                listeners: {

                    // Show a tooltip when a validation error occurs
                    onFieldError: function (elem, constraints, parsleyField) {
                        console.log("consoleLog");
                        elem.tooltip({
                            animation: false,
                            container: 'body',
                            placement: 'top',
                            title: elem.data('error-message')
                        });
                    },
                    // Hide validation tooltip if field is validated
                    onFieldSuccess: function (elem, constraints, parsleyField) {
                        alert("eleme");
                        elem.tooltip('destroy');
                    }
                }
            };
            var ok = form.parsley(config).validate();
            form.find('.bs-callout-validationfail').eq(0).toggleClass('hidden', ok);
            return ok;
        }

        return true;

    }

    

}(jQuery));

