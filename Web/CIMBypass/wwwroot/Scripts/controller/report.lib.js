﻿(function ($) {

    $.fn.reportize = function(options) {
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
            id: $('input[data-reportid]').val(),
            urlSearch: $('[data-reportbutton]').data('reportactionsearch'),
            urlLoadFilter: 'http://localhost/WebReport3/Report/LoadFilter'
        }, options);

        $('[data-filter][data-children]').off('change').on('change', function () {
            var names = $(this).data('children').split(',');
            for (var i = 0; i < names.length; i++) {
                this.loadFilter("[name='" + names[i] + "']");
            }
        });

        $('[data-reportbutton').off('click').on('click', this.search());

        var report = {
            serializeReportFilter: function (selector) {
                var filter = selector ? $("[data-filter]", selector) : $("[data-filter]");
                var result = [];
                filter.each(function (i, item) {

                    var json = { "Name": $(item).attr('name'), "Value": $(item).val(), "ValueType": $(item).data('valuetype') };
                    if ($(item).data('parents')) {
                        var names = $(item).data('parents')
                            .split(',')
                            .map(function (n) { return "[name='" + n + "']"; })
                            .join();

                        var parents = this.serializeReportFilter(names);
                        json.Parents = { "Filters": parents };
                    }

                    result.push(json);
                });

                return result;
            },
            buildTableByJson: function (json) {
                var columns = $(json.Columns).map(function (i, item) {
                    return { "data": item.Data, "type": item.Type };
                });


                /*"language": {
                    url: 'plugins/datatables/italian.json'
                },
                */
             
                $('[data-reportdata]').show();

                //genero datatable
                $("[data-reporttable]").html('').append(json.Header).DataTable({
                    "destroy": true,
                    "processing": true,
                    "searching": false,
                    "data": json.Rows
                });
            },
            search: function () {
                var data = JSON.stringify({
                    idReport: this.id,
                    filter: { Filters: this.serializeReportFilter() }
                });

                $.ajax({
                    traditional: true,
                    type: 'POST',
                    url: this.urlSearch,
                    data: data,
                    dataType: 'json',
                    contentType: 'application/json; charset=UTF-8',
                    success: function (result) {
                        this.buildTableByJson(result);
                    },
                });
            },
            loadFilter: function (selector) {
                var data = {
                    idReport: this.id,
                    filter: this.serializeReportFilter(selector)[0],
                };

                $.ajax({
                    traditional: true,
                    type: 'POST',
                    url: this.urlLoadFilter,
                    data: JSON.stringify(data),
                    dataType: 'json',
                    contentType: 'application/json; charset=UTF-8',
                    success: function (result) {
                        if (result.DataSource && flt.data('filter') == 'picklist') {
                            flt.html('');

                            $.each(result.DataSource, function (i, item) {
                                flt.append($('<option>', {
                                    value: item.Key,
                                    text: item.Value
                                }));
                            });
                        }
                        else {
                            flt.val(result.Value);
                        }

                        spinner.hide();
                    }
                });
            }
        }
    }

}(jQuery));