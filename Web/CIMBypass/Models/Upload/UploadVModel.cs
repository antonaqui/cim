﻿using Enel.Indra.CIMBypass.Business.Data.Model.Entity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Enel.Indra.CIMBypass.Models.Upload
{
    public class UploadVModel
    {
        public IEnumerable<int> flussoSelezionato { get; set; }
        public IEnumerable<SelectListItem> elencoFlussi { get; set; }

        public void InizializzaElencoFlussi(List<CatFlussiModel> elenco)
        {
            List<SelectListItem> ddlFlussoTmp;

            ddlFlussoTmp = (from c in elenco
                            select new SelectListItem()
                            {
                                Text = c.NomeFlusso,
                                Value = c.IdFlusso.ToString(),
                                Selected = false
                            }).ToList<SelectListItem>();

            this.elencoFlussi = ddlFlussoTmp;
        }
    }
}
