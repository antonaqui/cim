﻿using Enel.Indra.CIMBypass.Business.Data.Model.CustomModel;
using System.Collections.Generic;

namespace Enel.Indra.CIMBypass.Models.Upload
{
    public class DettaglioScartiVModel
    {
        public string flusso { get; set; }

        public string dataScheduling { get; set; }

        public string dataUpload { get; set; }

        public string versione { get; set; }

        //public List<GrigliaScartiCsvInputModel> elencoScarti { get; set; }
    }
}