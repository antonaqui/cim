﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enel.Indra.CIMBypass.Models.Common
{
    public class JsonResponse
    {
        public string message { get; set; }
        public string result { get; set; }
    }
}