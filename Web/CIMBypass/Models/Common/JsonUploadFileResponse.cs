﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enel.Indra.CIMBypass.Models.Common
{
    public class JsonUploadFileResponse
    {
        public JsonUploadFileResponse(string message, string isUploaded)
        {
            this.message = message;
            this.isUploaded = isUploaded;
        }

        public JsonUploadFileResponse()
        {
        }

        public string message { get; set; }
        public string isUploaded { get; set; }
        public int idArchivioCSV { get; set; }
        public bool asteDaily { get; set; }
    }
}