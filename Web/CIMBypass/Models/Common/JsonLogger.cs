﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enel.Indra.CIMBypass.Models.Common
{
    public class JsonLogger
    {
        public string value { get; set; }
        public bool isError { get; set; }
    }
}